package my;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestRendering {

	private TraversalFinder finder;

	@Before
	public void setUp() {
		finder = new TraversalFinder(new SeparatorFinder(), new DefaultAdjuster());
	}
	
	@Test
	public void testRenderingRectangle5x10() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(10, 5);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(72, rendering.size());
		
		System.out.println(rendering);
	}

	@Test
	public void testRenderingRectangle5x40() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(5, 40);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(312, rendering.size());
		
		System.out.println(rendering);
	}
	
	@Test
	public void testRenderingRectangle50x50() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(50, 50);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(49 * 49 * 2, rendering.size());
		
		System.out.println(rendering);
	}
	
	@Test
	public void testRenderingRectangle100x100() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(100, 100);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(99 * 99 * 2, rendering.size());
		
		System.out.println(rendering);
	}
	
	@Test
	@Ignore
	public void testRenderingRectangle1000x100() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(1000, 100);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(999 * 99 * 2, rendering.size());
		
		System.out.println(rendering);
	}
	
	@Test
	public void testRenderingRectangle10x10() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(10, 10);
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		Assert.assertEquals(162, rendering.size());
		
		System.out.println(rendering);
	}
	
	

}
