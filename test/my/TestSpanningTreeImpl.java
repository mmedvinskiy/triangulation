package my;

import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestSpanningTreeImpl {
	
	@Test
	public void testSpanningTree() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(4, 4);
		
		SpanningTreeImpl tree = SpanningTreeImpl.constructBreadFirstSpanningTree(graph, 0);
		
		Assert.assertEquals(16, tree.getVerticesCount());
		
		//Radius
		Assert.assertEquals(6, tree.getRadius());
		
		//Levels
		Assert.assertEquals(0, tree.getLevel(Integer.valueOf(0)));
		Assert.assertEquals(1, tree.getLevel(Integer.valueOf(1)));
		Assert.assertEquals(1, tree.getLevel(Integer.valueOf(4)));
		Assert.assertEquals(2, tree.getLevel(Integer.valueOf(2)));
		Assert.assertEquals(2, tree.getLevel(Integer.valueOf(5)));
		Assert.assertEquals(2, tree.getLevel(Integer.valueOf(8)));
		Assert.assertEquals(3, tree.getLevel(Integer.valueOf(3)));
		Assert.assertEquals(3, tree.getLevel(Integer.valueOf(6)));
		Assert.assertEquals(3, tree.getLevel(Integer.valueOf(9)));
		Assert.assertEquals(3, tree.getLevel(Integer.valueOf(12)));
		
		//Children
		List<Integer> children = tree.getChildren(Integer.valueOf(0));
		Assert.assertEquals(2, children.size());
		Assert.assertTrue(children.contains(Integer.valueOf(1)));
		Assert.assertTrue(children.contains(Integer.valueOf(4)));
		
		//Levels
		Collection<Integer> vertices = tree.getVertices(4);
		Assert.assertEquals(3, vertices.size());
		Assert.assertTrue(vertices.contains(Integer.valueOf(7)));
		Assert.assertTrue(vertices.contains(Integer.valueOf(10)));
		Assert.assertTrue(vertices.contains(Integer.valueOf(13)));
		
	};

	@Test
	public void testSpanningTree2() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(4, 4);
		SpanningTreeImpl tree = SpanningTreeImpl.constructBreadFirstSpanningTree(graph, 10);
		
		tree.outputLevels();
		tree.outputAssociatedCosts();
	}
}
