package my;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestSeparators {

	private final static Logger LOG = Logger.getLogger(TestSeparators.class);
	
	private SeparatorFinder finder;

	@Before
	public void setUp() {
		finder = new SeparatorFinder();
	}
	
	
	@Test
	public void testSeparatorRectangle1() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(1, 1);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		Assert.assertEquals(1, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(0)));
	}
	
	@Test
	public void testSeparatorRectangle2() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(2, 2);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		Assert.assertEquals(2, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(1)));
		Assert.assertTrue(separator.contains(Integer.valueOf(2)));
	}
	
	@Test
	public void testSeparatorRectangle3() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(3, 3);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		Assert.assertEquals(3, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(2)));
		Assert.assertTrue(separator.contains(Integer.valueOf(4)));
		Assert.assertTrue(separator.contains(Integer.valueOf(6)));
	}
	
	@Test
	public void testSeparatorRectangle4() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(4, 4);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		Assert.assertEquals(4, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(3)));
		Assert.assertTrue(separator.contains(Integer.valueOf(6)));
		Assert.assertTrue(separator.contains(Integer.valueOf(9)));
		Assert.assertTrue(separator.contains(Integer.valueOf(12)));
		
		System.out.println(separator);
		
	}

	@Test
	public void testSeparatorRectangle100x4() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(100, 4);

		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		System.out.println(separator);
		
		Assert.assertEquals(4, separator.size());
	}
	
	@Test
	public void testSeparatorRectangle10x5() {

		System.out.println("testSeparatorRectangle10x5");
		
		PlanarGraphImpl graph = GraphFactory.createRectangle(10, 5);
		
		Separation<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		System.out.println(separator);
		
		Assert.assertEquals(20, separation.getComponentA().size());
		Assert.assertEquals(25, separation.getComponentB().size());
		Assert.assertEquals(5, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(6)));
		Assert.assertTrue(separator.contains(Integer.valueOf(15)));
		Assert.assertTrue(separator.contains(Integer.valueOf(24)));
		Assert.assertTrue(separator.contains(Integer.valueOf(33)));
		Assert.assertTrue(separator.contains(Integer.valueOf(42)));
		
		PlanarGraph<Integer> subGraphA = new SubGraph(graph, separation.getComponentA());
		SeparationIfc<Integer> separationA = finder.findSeparator(subGraphA);
		System.out.println(separationA.getSeparator());

		PlanarGraph<Integer> subGraphB = new SubGraph(graph, separation.getComponentB());
		SeparationIfc<Integer> separationB = finder.findSeparator(subGraphB);
		System.out.println(separationB.getSeparator());
	}

	@Test
	public void testSeparatorSubGraphRectangle10x5() {

		System.out.println("testSeparatorSubGraphRectangle10x5");
		
		Set<Integer> vertices = new HashSet<Integer>();
		//[17, 16, 18, 7, 25, 8, 9, 27, 26]
		
		vertices.add(Integer.valueOf(7));
		vertices.add(Integer.valueOf(8));
		vertices.add(Integer.valueOf(9));
		vertices.add(Integer.valueOf(17));
		vertices.add(Integer.valueOf(16));
		vertices.add(Integer.valueOf(18));
		vertices.add(Integer.valueOf(25));
		vertices.add(Integer.valueOf(26));
		vertices.add(Integer.valueOf(27));
		
		SubGraph graph = new SubGraph(GraphFactory.createRectangle(10, 5), vertices);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		System.out.println(separator);
		
		/*Assert.assertEquals(20, separation.getComponentA().size());
		Assert.assertEquals(25, separation.getComponentB().size());
		Assert.assertEquals(5, separator.size());
		
		Assert.assertTrue(separator.contains(Integer.valueOf(6)));
		Assert.assertTrue(separator.contains(Integer.valueOf(15)));
		Assert.assertTrue(separator.contains(Integer.valueOf(24)));
		Assert.assertTrue(separator.contains(Integer.valueOf(33)));
		Assert.assertTrue(separator.contains(Integer.valueOf(42)));*/
		
	}
	
	@Test
	public void testSeparatorSubGraphRectangle10x50() {

		System.out.println("testSeparatorSubGraphRectangle10x50");
		
		Set<Integer> vertices = new HashSet<Integer>();
		//[17, 16, 18, 7, 25, 8, 9, 27, 26]
		
		vertices.add(Integer.valueOf(58));
		vertices.add(Integer.valueOf(59));
		vertices.add(Integer.valueOf(67));
		vertices.add(Integer.valueOf(68));
		vertices.add(Integer.valueOf(69));
		vertices.add(Integer.valueOf(77));
		vertices.add(Integer.valueOf(78));
		
		SubGraph graph = new SubGraph(GraphFactory.createRectangle(10, 50), vertices);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		System.out.println(separator);
	}
	
	@Test
	public void testSeparatorSubGraphRectangle100x6() {

		System.out.println("testSeparatorSubGraphRectangle100x6");
		
		Set<Integer> vertices = new HashSet<Integer>();
		//[17, 16, 18, 7, 25, 8, 9, 27, 26]
		
		vertices.add(Integer.valueOf(205));
		vertices.add(Integer.valueOf(204));
		vertices.add(Integer.valueOf(304));
		vertices.add(Integer.valueOf(5));
		vertices.add(Integer.valueOf(203));
		vertices.add(Integer.valueOf(6));
		vertices.add(Integer.valueOf(7));
		vertices.add(Integer.valueOf(403));
		vertices.add(Integer.valueOf(106));
		vertices.add(Integer.valueOf(104));
		vertices.add(Integer.valueOf(303));
		vertices.add(Integer.valueOf(105));
		
		SubGraph graph = new SubGraph(GraphFactory.createRectangle(100, 6), vertices);
		
		SeparationIfc<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		System.out.println(separator);
	}
	
	//205, 204, 304, 5, 203, 6, 7, 403, 106, 104, 303, 105
	
	@Test
	public void testSeparatorSubGraphRectangle50x50() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("testSeparatorSubGraphRectangle50x50");
		}

		PlanarGraph<Integer> graph = GraphFactory.createSample2();

		Integer vertex1170 = Integer.valueOf(1170);

		Separation<Integer> separation = finder.findSeparator(graph);
		Collection<Integer> separator = separation.getSeparator();
		
		Assert.assertTrue(separator.contains(vertex1170) || separation.getComponentA().contains(vertex1170) || separation.getComponentB().contains(vertex1170));
	}
	
	@Test
	public void testSeparatorSubGraphRectangle100x100() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("testSeparatorSubGraphRectangle100x100");
		}

		PlanarGraph<Integer> graph = GraphFactory.createSample3();

		Integer vertex2510 = Integer.valueOf(2510);
		Integer vertex2511 = Integer.valueOf(2511);

		Separation<Integer> separation = finder.findSeparator(graph);
		
		Collection<Integer> componentA = separation.getComponentA();
		Collection<Integer> componentB = separation.getComponentB();
		Collection<Integer> separator = separation.getSeparator();
		
		Collection<Integer> component2510 = componentA.contains(vertex2510) ? componentA : 
			(componentB.contains(vertex2510) ? componentB : separator);
		Collection<Integer> component2511 = componentA.contains(vertex2511) ? componentA : 
		    (componentB.contains(vertex2511) ? componentB : separator);
		
		
		Assert.assertTrue(component2510 == component2511);
	}
}
