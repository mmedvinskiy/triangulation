package my;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
    { 
        TestGraph.class,
        TestRendering.class,
        TestSeparators.class,
        TestSpanningTreeImpl.class
    })
public class TestSuite {

}


