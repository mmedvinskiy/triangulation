package my;

import java.util.LinkedList;

import junit.framework.Assert;

import my.GraphUtils.Costs;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestGraph {
	
	@Test
	public void testPlanarRepresentation() {
		
		PlanarGraphImpl graph = GraphFactory.createSample1();
		
		graph.outputAdjacencyLists();
	};
	
	@Test
	public void testMod() {
		Assert.assertEquals(0, 0 % 5);
		Assert.assertEquals(1, 1 % 5);
		Assert.assertEquals(2, 2 % 5);
		Assert.assertEquals(3, 3 % 5);
		Assert.assertEquals(4, 4 % 5);
		Assert.assertEquals(0, 5 % 5);
		Assert.assertEquals(1, 6 % 5);
	}
	
	@Test
	public void testShrunkenGraph() {
		
		PlanarGraphImpl graph = GraphFactory.createRectangle(4, 4);
		SpanningTreeImpl tree = SpanningTreeImpl.constructBreadFirstSpanningTree(graph, 0);
		
		tree.outputChildren();
		
		Assert.assertEquals(2, tree.getLevel(Integer.valueOf(8)));
		
		ShrunkenGraph shrunkenGraph = new ShrunkenGraph(graph, tree, 2, 10); 
		
		shrunkenGraph.outputAdjacencyLists();
		
		Assert.assertEquals(18, shrunkenGraph.getTriangles().size());
		System.out.println(shrunkenGraph.getTriangles());
	}
	
	@Test
	public void testCycleEvaluation() {

		PlanarGraphImpl graph = GraphFactory.createRectangle(4, 4);
		SpanningTreeImpl tree = SpanningTreeImpl.constructBreadFirstSpanningTree(graph, 0);
		
		tree.outputChildren();
		
		double delta = 0.1;
		
		Assert.assertEquals(16.0, tree.getDescendantCosts(tree.getRoot()), delta);

		Assert.assertEquals(15.0, tree.getDescendantCosts(Integer.valueOf(4)) + 
				                tree.getDescendantCosts(Integer.valueOf(1)), delta);
	
		Assert.assertEquals(13.0, tree.getDescendantCosts(Integer.valueOf(8)) + 
				                tree.getDescendantCosts(Integer.valueOf(5)) +
                                tree.getDescendantCosts(Integer.valueOf(2)), delta);
		
		{
			LinkedList<Integer> cycle = new LinkedList<Integer>();
			cycle.add(Integer.valueOf(1));
			cycle.add(Integer.valueOf(2));
			cycle.add(Integer.valueOf(6));
			cycle.add(Integer.valueOf(10));
			cycle.add(Integer.valueOf(14));
			cycle.add(Integer.valueOf(13));
			cycle.add(Integer.valueOf(9));
			cycle.add(Integer.valueOf(5));

			Costs costs = GraphUtils.evaluateCosts(graph, tree, cycle);
			Assert.assertEquals(0.0, costs.getInside(), delta);
			Assert.assertEquals(8.0, costs.getOutside(), delta);
		}
	}
		
}
