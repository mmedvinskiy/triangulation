package my;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SpanningTreeImpl implements SpanningTree<Integer> {

	private final int size;
	private final int root;
	private final int radius;

	private final int[] parent;

	private final int[] level;
	private final int[] descendantCosts;

	private final List[] children;

	// TODO: do we need sets or simply number of vertixes?
	private final Map<Integer, List<Integer>> levels;

	private SpanningTreeImpl(int size, int[] parent, int[] level,
			List[] children, int root, int radius,
			Map<Integer, List<Integer>> levels, int[] associatedCosts) {
		super();
		this.size = size;
		this.parent = parent;
		this.level = level;
		this.children = children;
		this.root = root;
		this.radius = radius;
		this.levels = levels;
		this.descendantCosts = associatedCosts;
	}

	@Override
	public int getNodesCount() {
		return size;
	}

	@Override
	public int getRadius() {
		return radius;
	}

	@Override
	public int getLevel(Integer node) {
		return level[node.intValue()];
	}

	@Override
	public List<Integer> getNodes(int level) {
		return levels.get(Integer.valueOf(level));
	}

	@Override
	public int getNumberOfNodes(int level) {
		return levels.get(Integer.valueOf(level)).size();
	}

	@Override
	public Integer getParent(Integer node) {
		return parent[node.intValue()] != -1 ? Integer.valueOf(parent[node
				.intValue()]) : null;
	}

	@Override
	public List<Integer> getChildren(Integer node) {
		return (List<Integer>) children[node.intValue()];
	}

	@Override
	public Integer getRoot() {
		return Integer.valueOf(root);
	}
	
	@Override
	public int getDescendantCosts(Integer node) {
		return descendantCosts[node.intValue()];
	}
	
	public static SpanningTreeImpl constructBreadFirstSpanningTree(
			PlanarGraph<Integer> graph, int root) {
		int size = graph.getNodesCount();

		int[] parent = new int[size];
		int[] level = new int[size];
		List[] children = new List[size];
		Map<Integer, List<Integer>> levels = new HashMap<Integer, List<Integer>>();

		for (int i = 0; i < size; i++) {
			level[i] = -1;
		}

		LinkedList<Integer> queue = new LinkedList<Integer>();

		queue.add(Integer.valueOf(root));
		parent[root] = -1;
		level[root] = 0;
		children[root] = new ArrayList<Integer>();

		int radius = 0;
		while (!queue.isEmpty()) {
			int node = queue.removeFirst().intValue();
			if (level[node] > radius) {
				radius = level[node];
			}

			Integer key = Integer.valueOf(level[node]);
			List<Integer> set = levels.get(key);
			if (set == null) {
				set = new ArrayList<Integer>();
				levels.put(key, set);
			}
			if (!set.contains(Integer.valueOf(node))) {
				set.add(Integer.valueOf(node));
			}

			Collection<Integer> adjacentNodes = graph.getAdjacentNodes(node);
			for (Integer adjacentNode : adjacentNodes) {
				if (level[adjacentNode] == -1) {

					if (children[node] == null) {
						children[node] = new ArrayList<Integer>();
					}

					children[node].add(adjacentNode);
					level[adjacentNode] = level[node] + 1;
					parent[adjacentNode] = node;

					queue.addLast(Integer.valueOf(adjacentNode));
				}
			}
		}

		int[] associatedCosts = new int[size];
		for (int i = radius; i >= 0; i--) {
			List<Integer> nodes = levels.get(Integer.valueOf(i));
			for (Integer node : nodes) {
				associatedCosts[node.intValue()] += 1;
				int p = parent[node.intValue()];
				if (p != -1) {
					associatedCosts[p] += associatedCosts[node.intValue()];
				}
			}
		}

		SpanningTreeImpl tree = new SpanningTreeImpl(size, parent, level,
				children, root, radius, levels, associatedCosts);
		return tree;
	}

	public void outputLevels() {
		System.out.println("Levels");
		for (Entry<Integer, List<Integer>> entry : levels.entrySet()) {
			System.out.print("Level " + entry.getKey() + ": ");

			List<Integer> list = entry.getValue();
			for (Integer node : list) {
				System.out.print(node + " ");
			}

			System.out.println();
		}
	}

	public void outputAssociatedCosts() {
		System.out.println("Associated costs");
		for (int i = 0; i < size; i++) {
			System.out.println("Cost " + i + ": " + descendantCosts[i]);
		}
	}

	public void outputChildren() {
		System.out.println("Children");
		for (int i = 0; i < size; i++) {
			System.out.print(i + ": ");
			List<Integer> nodes = children[i];
			if (nodes != null) {
				for (Integer node : nodes) {
					System.out.print(node + " ");
				}
			}
			System.out.println();
		}
	}


}
