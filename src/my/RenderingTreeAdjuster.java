package my;

public interface RenderingTreeAdjuster {
	
	public void adjustRenderingTree(RenderingTree tree, PlanarGraph<Integer> graph);
	
}
