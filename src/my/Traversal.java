package my;

import java.util.List;

public class Traversal {
    private final List<Triangle> path;
    private final int calculations;
    private final int cache; 
     
     
	public Traversal(List<Triangle> path, int calculations, int cache) {
		super();
		this.path = path;
		this.calculations = calculations;
		this.cache = cache;
	}

	public List<Triangle> getPath() {
		return path;
	}

	public int getCalculations() {
		return calculations;
	}

	public int getCache() {
		return cache;
	}
}
