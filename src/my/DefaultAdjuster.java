package my;

import java.util.Collection;
import java.util.List;

public class DefaultAdjuster implements RenderingTreeAdjuster{

	public void adjustRenderingTree(RenderingTree tree, PlanarGraph<Integer> graph) {
		if (!tree.hasGrandchildren()) {
			return;
		}
		
		RenderingTree child1 = tree.getChild1();
		RenderingTree child2 = tree.getChild2();
		
		RenderingTree[][] grandchildren = new RenderingTree[2][2]; 
		grandchildren[0][0] = child1.getChild1();
		grandchildren[0][1] = child1.getChild2();
		
		grandchildren[1][0] = child2.getChild1();
		grandchildren[1][1] = child2.getChild2();
		
		int index0 = 0;
		int index1 = 0;
		int max = 0;
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				int result = DefaultAdjuster.calculateCommonVertices(grandchildren[0][i].getComponent(), grandchildren[1][j].getComponent(), tree.getSeparator(), graph);
				if (result > max) {
					max = result;
					index0 = i;
					index1 = j;
				}
			}
		}
		
		if (index0 == 0) {
			child1.switchChidlren();
		}
		
		if (index1 == 1) {
			child2.switchChidlren();
		}
		
		adjustRenderingTree(child1, graph);
		adjustRenderingTree(child2, graph);
		
	}

	protected static int calculateCommonVertices(Collection<Integer> componentA,
			Collection<Integer> componentB, Collection<Integer> separator,
			PlanarGraph<Integer> graph) {
	
		int sum = 0;
		for (Integer vertex : separator) {
			List<Triangle> triangles = graph.getTriangles(vertex);
	
			boolean hasA = false;
			boolean hasB = false;
	
			for (Triangle triangle : triangles) {
	
				Integer second = Integer.valueOf(triangle.second(vertex.intValue()));
				Integer third = Integer.valueOf(triangle.third(vertex.intValue(), second.intValue()));
	
				if (componentA.contains(second) || componentA.contains(third)) {
					hasA = true;
				}
	
				if (componentB.contains(second) || componentB.contains(third)) {
					hasB = true;
				}
			}
			if (hasA && hasB) {
				sum++;
			}
	
		}
		return sum;
	}

}
