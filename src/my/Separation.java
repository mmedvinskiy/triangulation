package my;

import java.util.Collection;

public class Separation<V> implements SeparationIfc<V> {
	
	private Collection<V> componentA;
	private Collection<V> componentB;
	private final Collection<V> separator;

	public Separation(Collection<V> componentA, Collection<V> componentB, Collection<V> separator) {
		super();
		this.componentA = componentA;
		this.componentB = componentB;
		this.separator = separator;
	}

	public Collection<V> getComponentA() {
		return componentA;
	}

	public Collection<V> getComponentB() {
		return componentB;
	}

	@Override
	public Collection<V> getSeparator() {
		return separator;
	}
	
	/*
	@Override
	public void switchAandB() {
		Collection<V> oldComponentA = componentA;
		componentA = componentB;
		componentB = oldComponentA;
	}*/
}
