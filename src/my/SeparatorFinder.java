package my;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.apache.log4j.Logger;

import my.GraphUtils.Costs;
import my.ShrunkenGraph.ShrunkenTree;

public class SeparatorFinder implements SeparatorFinderIfc {
	
	private final static Logger LOG = Logger.getLogger(SeparatorFinder.class);
	
	public SeparatorFinder()
	{
		
	}
	
	@Override
	public Separation<Integer> findSeparator(PlanarGraph<Integer> graph) {
		
		if (LOG.isTraceEnabled()) {
		    LOG.trace("findSeparator(): " + graph.getVertices());
		}
				
		//STEP 3
		SpanningTreeImpl tree = SpanningTreeImpl.constructBreadFirstSpanningTree(graph, graph.getStartVertex().intValue());
		
		int n = graph.getVerticesCount();
		
		if (tree.getVerticesCount() < n) {
			
			//Graph is not connected
			Set<Integer> separator = new HashSet<Integer>();
			
			Collection<Integer> componentA = tree.getVertices();
			
			Set<Integer> componentB = new HashSet<Integer>(graph.getVertices());
			componentB.removeAll(componentA);
			
			Separation<Integer> separation = new Separation<Integer>(componentA, componentB, separator);
			return separation;
		}
		
		
		//STEP 4
		int l1 = -1;
		int k = 0;
		{
			double threshold = n / 2.0;

			while (k < threshold) {
				l1++;
				k = k + tree.getNumberOfVertices(l1);
			}
		}		
		if (LOG.isTraceEnabled()) {
		    LOG.trace("L1: " + l1);
		}
		
		//STEP 5.1
		int l0 = l1;

		{
			double threshold = 2 * Math.sqrt((double) k);
			while ((tree.getNumberOfVertices(l0) + 2 * (l1 - l0) > threshold)
					&& (l0 > -1)) {
				l0--;
			}

			if (l0 == -1) {
				throw new IllegalStateException("Can not find L0: unreachable statement!");
			}
		}
		if (LOG.isTraceEnabled()) {
			LOG.trace("LO: " + l0);
		}
		
		if (l0 == l1) {
			
			Set<Integer> componentA = new HashSet<Integer>();
			for (int i = 0; i < l0; i++) {
				componentA.addAll(tree.getVertices(i));
			}
			
			Set<Integer> componentB = new HashSet<Integer>();
			for (int i = l0 + 1; i < tree.getRadius() + 1; i++) {
				componentB.addAll(tree.getVertices(i));
			}
			
			Set<Integer> separator = new HashSet<Integer>();
			separator.addAll(tree.getVertices(l0));
			
			Separation<Integer> separation = new Separation<Integer>(componentA, componentB, separator); 
			return separation;
		}
		
		//STEP 5.2
		int l2 = l1 + 1;

		{
			double threshold = 2 * Math.sqrt((double) (n - k));
			while ((tree.getNumberOfVertices(l2) + 2 * (l2 - l1 - 1) > threshold)
					&& (l2 < tree.getRadius())) {
				l2++;
			}

			if (l2 == tree.getRadius() + 2) {
				throw new IllegalStateException("Can not find L2: unreachable statement!");
			}
		}
		if (LOG.isTraceEnabled()) {
			LOG.trace("L2: " + l2);
		}
		
		//STEP 6
		ShrunkenGraph shrunkenGraph = new ShrunkenGraph(graph, tree, l0, l2);
		
		ShrunkenTree shrunkenTree = shrunkenGraph.getTree();
		
		Integer cutVertex = shrunkenGraph.getCutVertex();
		Integer root = shrunkenTree.getRoot();
		
		if (cutVertex != null) {
			if (cutVertex.equals(root)) {
				Separation<Integer> sep = shrunkenGraph.separateByCutVertex();
				return createSeparation(tree, l0, l2, root, sep);
			}else{
				throw new IllegalStateException("Algorithm is not yet implemented");
			}
			
		}
		
		//TODO: debug code
		//shrunkenGraph.outputAdjacencyLists();
		
		//STEP7
		Edge edge = shrunkenGraph.getArbitraryNonTreeEdge();
		LinkedList<Integer> cycle = shrunkenGraph.findCycle(edge);
		
		
		double threshold = 2.0 / 3.0;
		Costs costs = GraphUtils.<Integer>evaluateCosts(shrunkenGraph, shrunkenTree, cycle);
		
		while ((costs.getInside() > threshold) || (costs.getOutside() > threshold)) {
			throw new IllegalStateException("Algorithm is not yet implemented");
		}
		
		Separation<Integer> sep = shrunkenGraph.separateByCycle(cycle);
		
		return createSeparation(tree, l0, l2, root, sep);
	}

	private static Separation<Integer> createSeparation(SpanningTreeImpl tree,
			int l0, int l2, Integer root, Separation<Integer> shrunkenGraphSeparation) {
		Collection<Integer> large;
		Collection<Integer> small;
		if (shrunkenGraphSeparation.getComponentA().size() > shrunkenGraphSeparation.getComponentB().size()) {
			large = shrunkenGraphSeparation.getComponentA();
			small = shrunkenGraphSeparation.getComponentB();
		}else{
			large = shrunkenGraphSeparation.getComponentB();
			small = shrunkenGraphSeparation.getComponentA();
		}
		
		Collection<Integer> componentA = large;
		Set<Integer> separator = new HashSet<Integer>();
		{
			separator.addAll(shrunkenGraphSeparation.getSeparator());
			separator.remove(root);
			separator.addAll(tree.getVertices(l0));
			separator.addAll(tree.getVertices(l2));
		}
		
		Set<Integer> componentB = new HashSet<Integer>();
		{
			for (int i = 0; i < l0; i++) {
				componentB.addAll(tree.getVertices(i));
			}

			for (int i = l2 + 1; i <= tree.getRadius(); i++) {
				componentB.addAll(tree.getVertices(i));
			}
			
			componentB.addAll(small);
		}
		
		Separation<Integer> separation = new Separation<Integer>(componentA, componentB, separator);
		return separation;
	}

	@Override
	public PlanarGraph<Integer> generateSubGraphB(PlanarGraph<Integer> graph,
			SeparationIfc<Integer> separation) {
		return new SubGraph(graph, ((Separation<Integer>)separation).getComponentB());
	}

	@Override
	public PlanarGraph<Integer> generateSubGraphA(PlanarGraph<Integer> graph,
			SeparationIfc<Integer> separation) {
		return new SubGraph(graph, ((Separation<Integer>)separation).getComponentA());
	}

}
