package my;

import java.util.List;

import org.apache.log4j.BasicConfigurator;

import my.sandbox.RectangleSeparationFinder;
import my.sandbox.RectangleSubGraph;

public class Main {
	
	public static int N = 1000000;
	
	public static void main(String[] args)
	{
		
		BasicConfigurator.configure();
		
		int width = 101;
		int height = 101;
		
		PlanarGraphImpl graph = GraphFactory.createRectangle(width, height);	
		Coordinates coordinates = GraphFactory.createCoordinates(width, height);
		TraversalFinder finder = new TraversalFinder(new SeparatorFinder(), new DefaultAdjuster());
		
		/*RectangleSubGraph graph = new RectangleSubGraph(width, height, 0, 0, width, height);	
		Coordinates coordinates = GraphFactory.createCoordinates(width + 1, height + 1);	
		TraversalFinder finder = new TraversalFinder(new RectangleSeparationFinder(), new DefaultAdjuster());*/
		
		List<Triangle> rendering = finder.generateMinimumStackRendering(graph);
		int cache = 30;
		int costs = TraversalFinder.evaluatePathCosts(rendering, cache);
		
	    Traversal traversal = new Traversal(rendering, costs, cache);

		MainFrame frame = new MainFrame();
		frame.setGraphAndPath(coordinates, traversal, graph);
    	frame.setVisible(true);
	}

}
