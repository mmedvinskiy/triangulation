package my;

public class Triangle {
	
	public int vertex1;
	public int vertex2;
	public int vertex3;
	
	public Triangle(int vertex1, int vertex2, int vertex3) {
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.vertex3 = vertex3;
	}
	
	public boolean contains(int vertex) {
		return vertex1 == vertex || vertex2 == vertex || vertex3 == vertex;
	}

	public int third(int first, int second) {
		if (vertex1 == first) {
			if (vertex2 == second) {
				return vertex3;
			}
			if (vertex3 == second) {
				return vertex2;
			}
		}

		if (vertex2 == first) {
			if (vertex1 == second) {
				return vertex3;
			}
			if (vertex3 == second) {
				return vertex1;
			}
		}

		if (vertex3 == first) {
			if (vertex1 == second) {
				return vertex2;
			}
			if (vertex2 == second) {
				return vertex1;
			}
		}
		
		throw new IllegalStateException("Vertices " + first + ", " + second + " were not found in triangle");
	}
	
	public int second(int vertex) {
		
		if (vertex1 == vertex) {
			return vertex2;
		}
		if (vertex2 == vertex) {
			return vertex3;
		}
		if (vertex3 == vertex) {
			return vertex1;
		}

		throw new IllegalStateException("Vertex " + vertex + " was not found in triangle");
	}
	
	public int getClockwiseVertex(int vertex) {
		if (vertex == vertex1) {
			return vertex3;
		}
		if (vertex == vertex2) {
			return vertex1;
		}
		if (vertex == vertex3) {
			return vertex2;
		}
		throw new IllegalStateException("Vertex " + vertex + "is not found in trianle " + toString());
	}

	public int getCounterClockwiseVertex(int vertex) {
		if (vertex == vertex1) {
			return vertex2;
		}
		if (vertex == vertex2) {
			return vertex3;
		}
		if (vertex == vertex3) {
			return vertex1;
		}
		throw new IllegalStateException("Vertex " + vertex + "is not found in trianle " + toString());
	}
	
	public void rearrange(int vertex1, int vertex2, int vertex3) {
		
		checkArgument(vertex1);
		checkArgument(vertex2);
		checkArgument(vertex3);
		
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.vertex3 = vertex3;
	}

	private void checkArgument(int vertex) {
		if (!contains(vertex)) {
			throw new IllegalArgumentException("Triangle " + toString() + "doesn't contain vertex " + vertex); 
		}
	}
	
	/*
	@Override
	public int hashCode() {
		return vertex1 + vertex2 + vertex3;
	}*/
		
	@Override
	public String toString() {
		return "Triangle [" + vertex1 + ", " + vertex2
				+ ", " + vertex3 + "]";
	}
	
	

}
