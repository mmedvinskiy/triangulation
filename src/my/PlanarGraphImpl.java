package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

//WE ASSUME THIS GRAPH iS CONNECTED
public class PlanarGraphImpl extends AbstractPlanarGraph {

	private final static Logger LOG = Logger.getLogger(PlanarGraphImpl.class);
	
	private final int verticesCount;
	private final List<Triangle> triangles;

	private final List[] adjacencyList;
	private final List[] trianglesList;
	
	private PlanarGraphImpl(int verticesCount) {

		this.verticesCount = verticesCount;
		triangles = new ArrayList<Triangle>();

		adjacencyList = new List[verticesCount];
		trianglesList = new List[verticesCount];

		for (int i = 0; i < verticesCount; i++) {
			adjacencyList[i] = new ArrayList<Integer>();
			trianglesList[i] = new ArrayList<Triangle>();
		}

	}

	public PlanarGraphImpl(int size, Triangle... triangles) {
		this(size);

		for (Triangle triangle : triangles) {
			addTriangle(triangle);
		}
		
		makePlanarRepresentation();
	}

	public PlanarGraphImpl(int size, List<Triangle> triangles) {
		this(size);

		for (Triangle triangle : triangles) {
			addTriangle(triangle);
		}

		makePlanarRepresentation();
	}
	
	public void addTriangle(Triangle triangle) {

		super.addTriangle(triangle);

		triangles.add(triangle);
		
		trianglesList[triangle.vertex1].add(triangle);
		trianglesList[triangle.vertex2].add(triangle);
		trianglesList[triangle.vertex3].add(triangle);
	}

	@Override
	public List<Triangle> getTriangles() {
		return triangles;
	}

	public List<Triangle> getTriangles(int vertex) {
		return trianglesList[vertex];
	}

	public List<Integer> getAdjacentVertices(Integer vertex) {
		return adjacencyList[vertex.intValue()];
	}

	protected void registerEdge(Integer vertex1, Integer vertex2) {

		List<Integer> list = adjacencyList[vertex1.intValue()];
		
		if (!list.contains(vertex2)) {
			list.add(vertex2);
		}
	}
	
	@Override
	public int getVerticesCount() {
		return verticesCount;
	}
	
	@Override
	public void outputAdjacencyLists() {
		if (LOG.isDebugEnabled()) {
			for (int i = 0; i < verticesCount; i++) {
				StringBuilder builder = new StringBuilder().append(i).append(
						": ");

				List<Integer> list = adjacencyList[i];
				for (Integer vertex : list) {
					builder.append(vertex).append(" ");
				}

				LOG.debug(builder.toString());
			}
		}
	}

	@Override
	public boolean exist(Integer vertex) {
		if (vertex == null) {
			return false;
		}
		
		return (0 <= vertex.intValue()) && (vertex.intValue() < verticesCount);
	}

	@Override
	public Integer getStartVertex() {
		return Integer.valueOf(0);
	}

	@Override
	public List<Triangle> getTriangles(Integer vertex) {
		return trianglesList[vertex.intValue()];
	}

	@Override
	protected void setAdjacencyList(Integer vertex, List<Integer> list) {
		adjacencyList[vertex.intValue()] = list;
	}

	@Override
	public double getWeight(Integer vertex) {
		return 1.0 / ((double)verticesCount);
	}

	@Override
	public Collection<Integer> getVertices() {
		return new HashSet<Integer>();
		//throw new IllegalStateException("This method is not yet implemented");
	}

}
