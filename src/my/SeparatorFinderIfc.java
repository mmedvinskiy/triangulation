package my;

public interface SeparatorFinderIfc {

	public SeparationIfc<Integer> findSeparator(PlanarGraph<Integer> graph);

	public PlanarGraph<Integer> generateSubGraphA(PlanarGraph<Integer> graph, SeparationIfc<Integer> separation);

	public PlanarGraph<Integer> generateSubGraphB(PlanarGraph<Integer> graph, SeparationIfc<Integer> separation);

}