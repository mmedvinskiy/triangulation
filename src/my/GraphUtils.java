package my;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GraphUtils {

	//TODO: cycle should not contain two same vertices
	public static <V> Costs evaluateCosts(PlanarGraph<V> graph, SpanningTree<V> tree, LinkedList<V> cycle) {
		
		if (cycle.size() < 3) {
			throw new IllegalArgumentException("Cycle contains less than 3 vertices");
		}
		
		double inside = 0;
		double outside = 0;
		
		Iterator<V> iterator = cycle.iterator();
		V first = iterator.next();
		
		V prev = cycle.getLast();
		V current = first;
		V next = iterator.next();
		
		do {
			if (next.equals(prev)) {
				throw new IllegalStateException("next == prev, cycle = " + cycle);
			}
			
			List<V> adjacentVertices = graph.getAdjacentVertices(current);
			int size = adjacentVertices.size();
			
			int i = (adjacentVertices.indexOf(next) + 1) % size;
			
			V adjacentVertex = adjacentVertices.get(i);
			
			InOutside inOutside = InOutside.IN;
			while (!adjacentVertex.equals(next)) {
				
				if (adjacentVertex.equals(prev)) {
					inOutside = InOutside.OUT;
				}else{
				 
				double increment = 0;
				
				if (current.equals(tree.getParent(adjacentVertex))) {
					increment = tree.getDescendantCosts(adjacentVertex);
				}else if (adjacentVertex.equals(tree.getParent(current))) {
					increment = tree.getDescendantCosts(tree.getRoot()) - tree.getDescendantCosts(current);
				}
				
				if (inOutside == InOutside.IN) {
					inside += increment;
				} else {
					outside += increment;
				}
				}
				i = (i + 1) % size;
				adjacentVertex = adjacentVertices.get(i);
			}

			prev = current;
			current = next;
			next = iterator.hasNext() ? iterator.next() : first;
		} while (current != first);
		
		Costs costs = new Costs(inside, outside);
		return costs;
	}
	
	public static class Costs{
		private final double inside;
		private final double outside;
		
		public Costs(double inside, double outside) {
			super();
			this.inside = inside;
			this.outside = outside;
		}

		public double getInside() {
			return inside;
		}

		public double getOutside() {
			return outside;
		}
	}
	
	public static enum InOutside{
		IN,
		
		OUT
	}

}
