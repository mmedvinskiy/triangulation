package my;

import java.util.Collection;
import java.util.List;

public interface SpanningTree<V> {
	
	public V getRoot();
	
	public int getVerticesCount();
	
	public int getRadius();
	
	public int getLevel(V vertex);
	
	public Collection<V> getVertices();
	
	public List<V> getVertices(int level);
	
	public int getNumberOfVertices(int level);
	
	public V getParent(V vertex);
	
	public List<V> getChildren(V vertex);
	
	public double getDescendantCosts(V vertex);

}
