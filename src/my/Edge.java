package my;

public class Edge {

	private final int p1;
	private final int p2;
	
	public int getP1() {
		return p1;
	}

	public int getP2() {
		return p2;
	}

	public Edge(int p1, int p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
		
/*	public Edge(int p1, int p2) {
		if (p1 < p2) {
			this.p1 = p1;
			this.p2 = p2;
		} else {
			this.p1 = p2;
			this.p2 = p1;
		}
	}*/

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + p1;
		result = prime * result + p2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (p1 != other.p1)
			return false;
		if (p2 != other.p2)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + p1 + "; " + p2 + "]";
	}
	
	public Edge invert() {
		return new Edge(p2, p1);
	}
}
