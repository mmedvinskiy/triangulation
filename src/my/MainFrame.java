package my;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import my.Coordinates.Point;

public class MainFrame extends JFrame{
	
	private static final long serialVersionUID = 4375754276466511033L;
	private static final int BORDER = 10;
	
	private JLabel label;
	private JPanel panel;
	private JLabel labelTriangle;
	
	private Traversal traversal;
	private Coordinates coordinates;
	private PlanarGraph<Integer> graph;
	
	private Triangle choosen;

	public MainFrame() throws HeadlessException {
		super("Frame");
		
		initUI();
		initListeners();
	}
	
	public void setGraphAndPath(Coordinates coordinates, Traversal traversal, PlanarGraph<Integer> graph) {
		this.coordinates = coordinates;
		this.graph = graph;
		this.traversal = traversal;
		
		label.setText("Calculations: " + traversal.getCalculations() + "  Cache: " + traversal.getCache() + "  Vertices: " + graph.getVerticesCount() + "  Triangles: " + graph.getTriangles().size());
		
		repaint();
	}

	private void initListeners() {
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
		
		MouseAdapter mouseListener = new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent arg0) {
				Triangle t = findTriangle(arg0);
				choosen = t;
				MainFrame.this.repaint();
			}

			@Override
			public void mouseMoved(MouseEvent arg0) {
				Triangle t = findTriangle(arg0);
				labelTriangle.setText(t != null ? String.valueOf(t) : "No selected triangle");
			}
			
		};
		
		panel.addMouseListener(mouseListener);
		panel.addMouseMotionListener(mouseListener);
		
	}
	
	private Triangle findTriangle(MouseEvent arg0) {
		
		Triangle t = null;
		double x = unscaleX(arg0.getPoint().getX());
		double y = unscaleY(arg0.getPoint().getY());
		
		for (Triangle triangle : traversal.getPath()) {
			Point cm = coordinates.getCenterMass(triangle);
			if (distance(cm, x, y) < 0.3) {
				t = triangle;
			}
		}
		return t;
	}
	
	private static double distance(Point point1, double x, double y) {
		return Math.sqrt((point1.x - x) * (point1.x - x) + (point1.y - y) * (point1.y - y));  
	}

	private void initUI() {
		panel = new ContentPanel();
		label = new JLabel();
		labelTriangle = new JLabel();
		
		setLayout(new BorderLayout());
		add(panel, BorderLayout.CENTER);
		add(label, BorderLayout.NORTH);
		add(labelTriangle, BorderLayout.SOUTH);
		
		setSize(new Dimension(800, 800));
	}
	
	public class ContentPanel extends JPanel{

		private static final long serialVersionUID = 2429446430272279983L;

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			Triangle previous = null;
			for (Triangle triangle : traversal.getPath()) {
				drawTriangle(g, triangle, previous);
				previous = triangle;
				
				if (triangle == choosen) break;
			}

			/*
			for (Set<Edge> border : traversal.getBorders()) {
				for (Edge edge : border) {
					drawEdge(g, edge);
				}
			}*/
		}
	}
	
	private void drawTriangle(Graphics g, Triangle current, Triangle previous) {
		int x1 = coordinates.getX(current.vertex1);
		int y1 = coordinates.getY(current.vertex1);

		int x2 = coordinates.getX(current.vertex2);
		int y2 = coordinates.getY(current.vertex2);

		int x3 = coordinates.getX(current.vertex3);
		int y3 = coordinates.getY(current.vertex3);
		
		g.setColor(Color.BLACK);
		
		drawLine(g, x1, y1, x2, y2);
		drawLine(g, x2, y2, x3, y3);
		drawLine(g, x1, y1, x3, y3);
		
		if (previous != null) {
			
			g.setColor(Color.RED);
			
			Point cm1 = coordinates.getCenterMass(previous);
			Point cm2 = coordinates.getCenterMass(current);
			
			drawLine(g, cm1.x, cm1.y, cm2.x, cm2.y);
		}
	}

	private void drawEdge(Graphics g, Edge edge) {
		int x1 = coordinates.getX(edge.getP1());
		int y1 = coordinates.getY(edge.getP1());

		int x2 = coordinates.getX(edge.getP2());
		int y2 = coordinates.getY(edge.getP2());

		g.setColor(Color.BLUE);
		
		drawLine(g, x1, y1, x2, y2);
	}
	
	private void drawLine(Graphics g, double x1, double y1, double x2, double y2) {
		
		g.drawLine(scaleX(x1), scaleY(y1), scaleX(x2), scaleY(y2));
		
	};
	
	private int scaleX(double x) {
		return (int)(BORDER + x * ((double)(panel.getWidth() - 2 * BORDER) / (double)coordinates.getMaxX())); 
	}

	private double unscaleX(double x) {
		return (double)(x - BORDER) * (double)coordinates.getMaxX() / (double)(panel.getWidth() - 2 * BORDER); 
	}
	
	private int scaleY(double y) {
		return (int)(BORDER + y * ((double)(panel.getHeight() - 2 * BORDER) / (double)coordinates.getMaxY())); 
	}

	private double unscaleY(double y) {
		return (double)(y - BORDER) * (double)coordinates.getMaxY() / (double)(panel.getHeight() - 2 * BORDER); 
	}
	
}
