package my;

import java.util.Collection;
import java.util.List;

//V should be comparable
public interface PlanarGraph<V> {

	public int getVerticesCount();
	
	public V getStartVertex();

	public Collection<V> getVertices();
	
	//Order is important for planar representation
	public List<V> getAdjacentVertices(V vertex);
	
	public boolean exist(V vertex);
	
	public Collection<Triangle> getTriangles();
	
	public List<Triangle> getTriangles(V vertex);

	//TODO: this method should be extracted to Utils class
	public void outputAdjacencyLists();
	
	public double getWeight(V vertex);

}