package my;

import java.math.BigInteger;

public class Factorial {

	public static void main(String[] args) {

		for (int i = 1; i < 12; i++) {

			System.out.println(factorial(i));
		}

	}

	public static BigInteger factorial(int n) {
		BigInteger result = BigInteger.valueOf(1);

		for (int i = 1; i <= n; i++) {
			result = result.multiply(BigInteger.valueOf(i));
		}

		return result;
	}

}
