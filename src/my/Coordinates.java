package my;

public class Coordinates {

	private final int[] x;
	private final int[] y;
	
	private int maxX;
	private int maxY;
	
	public Coordinates(int size) {

		x = new int[size];
		y = new int[size];
	}
	
	public void setXY(int vertex, int x, int y) {
		this.x[vertex] = x;
		this.y[vertex] = y;
	}
	
	public int getX(int vertex) {
		return x[vertex];
	}

	public int getY(int vertex) {
		return y[vertex];
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}
	
	public Point getCenterMass(Triangle triangle) {
		double coordX = (double)(x[triangle.vertex1] + x[triangle.vertex2] + x[triangle.vertex3]) / 3;
		double coordY = (double)(y[triangle.vertex1] + y[triangle.vertex2] + y[triangle.vertex3]) / 3;
		return new Point(coordX, coordY);
	}
	
	public static class Point{
		public double x;
		public double y;
		
		public Point(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}
	}

}
