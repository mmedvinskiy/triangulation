package my;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

public class RenderingTree {
	
	private final static Logger LOG = Logger.getLogger(RenderingTree.class);
	
	private final Collection<Integer> component;
	private Collection<Integer> separator;

	private RenderingTree child1 = null;
	private RenderingTree child2 = null;
	
	public RenderingTree(Collection<Integer> component) {
		super();
		this.component = component;
	}

	public RenderingTree getChild1() {
		return child1;
	}
	public void setChild1(RenderingTree child1) {
		this.child1 = child1;
	}
	public RenderingTree getChild2() {
		return child2;
	}
	public void setChild2(RenderingTree child2) {
		this.child2 = child2;
	}
	public Collection<Integer> getComponent() {
		return component;
	}
	public Collection<Integer> getSeparator() {
		return separator;
	}
	public void setSeparator(Collection<Integer> separator) {
		this.separator = separator;
	}
	
	public boolean hasChildren() {
		return (child1 != null) && (child2 != null);
	}
	
	public boolean hasChild1() {
		return (child1 != null);
	}
	
	public boolean hasChild2() {
		return (child2 != null);
	}
	
	public boolean hasGrandchildren() {
		if (!hasChildren()) {
			return false;
		}
		return child1.hasChildren() && (child2.hasChildren());
	}
	
	public void switchChidlren() {
		RenderingTree oldChild1 = child1;
		child1 = child2;
		child2 = oldChild1;
	}

	/*
	@Override
	public String toString() {
		//return toString(0).toString();
	}*/
	
	public boolean isEmpty() {
		return (child1 == null) && (child2 == null) && (separator == null);
	}
	
	public void log(Priority priority) {
		if (LOG.isEnabledFor(priority)) {
			log(0, priority);
		}
	}
	
	private void log(int space, Priority priority) {
		StringBuilder spaceBuilder = new StringBuilder();
		for (int i = 0; i < space; i++) {
			spaceBuilder.append(" ");
		}
		
		if (component.size() > 0) {
			LOG.log(priority,
					new StringBuilder().append(spaceBuilder)
							.append("Start vertex: ")
							.append(component.iterator().next()).toString());
		}
		LOG.log(priority,
				new StringBuilder().append(spaceBuilder).append("Vertices: ")
						.append(component).toString());
		LOG.log(priority,
				new StringBuilder().append(spaceBuilder).append("Separator: ")
						.append(separator).toString());

		if (child1 != null) {
			child1.log(space + 1, priority);
		}
		if (child2 != null) {
			child2.log(space + 1, priority);
		}
	}
	
	
}
