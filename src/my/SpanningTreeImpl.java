package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

public class SpanningTreeImpl implements SpanningTree<Integer> {

	private final static Logger LOG = Logger.getLogger(SpanningTreeImpl.class);

	private final int verticesCount;
	private final int root;
	private final int radius;

	private final Set<Integer> vertices;
	private final Map<Integer, Integer> parent;

	private final Map<Integer, Integer> level;
	private final Map<Integer, Double> descendantCosts;

	private final Map<Integer, List<Integer>> children;

	private final Map<Integer, List<Integer>> levels;

	private SpanningTreeImpl(int verticesCount, Map<Integer, Integer> parent, Map<Integer, Integer> level,
			Map<Integer, List<Integer>> children, int root, int radius,
			Map<Integer, List<Integer>> levels, Map<Integer, Double> descendantCosts, Set<Integer> vertices) {
		super();
		this.verticesCount = verticesCount;
		this.parent = parent;
		this.level = level;
		this.children = children;
		this.root = root;
		this.radius = radius;
		this.levels = levels;
		this.descendantCosts = descendantCosts;
		this.vertices = vertices;
	}

	@Override
	public int getVerticesCount() {
		return verticesCount;
	}

	@Override
	public int getRadius() {
		return radius;
	}

	@Override
	public int getLevel(Integer vertex) {
		return level.get(vertex).intValue();
	}

	@Override
	public List<Integer> getVertices(int level) {
		List<Integer> list = levels.get(Integer.valueOf(level));
		return list != null ? list : Collections.<Integer>emptyList();
	}

	@Override
	public int getNumberOfVertices(int level) {
		List<Integer> list = levels.get(Integer.valueOf(level));
		return list == null ? 0 : list.size();
	}

	@Override
	public Integer getParent(Integer vertex) {
		return parent.get(vertex);
	}

	@Override
	public List<Integer> getChildren(Integer vertex) {
		List<Integer> list = children.get(vertex);
		return list != null ? list : Collections.<Integer>emptyList();
	}

	@Override
	public Integer getRoot() {
		return Integer.valueOf(root);
	}
	
	@Override
	public double getDescendantCosts(Integer vertex) {
		return descendantCosts.get(vertex).doubleValue();
	}
	
	public static SpanningTreeImpl constructBreadFirstSpanningTree(
			PlanarGraph<Integer> graph, int root) {

		Set<Integer> vertices = new HashSet<Integer>();
		
		Map<Integer, Integer> parent = new HashMap<Integer, Integer>();
		Map<Integer, Integer> level = new HashMap<Integer, Integer>();
		Map<Integer, List<Integer>> children = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> levels = new HashMap<Integer, List<Integer>>();

		/*for (int i = 0; i < size; i++) {
			level[i] = -1;
		}*/

		LinkedList<Integer> queue = new LinkedList<Integer>();

		Integer rootVertex = Integer.valueOf(root);
		queue.add(rootVertex);
		vertices.add(rootVertex);
		level.put(rootVertex, Integer.valueOf(0));
		children.put(rootVertex, new ArrayList<Integer>());
		int verticesCount = 1;

		int radius = 0;
		while (!queue.isEmpty()) {
			Integer vertex = queue.removeFirst();
			
			Integer key = level.get(vertex);
			int vertexLevel = key.intValue();
			
			if (vertexLevel > radius) {
				radius = vertexLevel;
			}

			List<Integer> set = levels.get(key);
			if (set == null) {
				set = new ArrayList<Integer>();
				levels.put(key, set);
			}
			if (!set.contains(vertex)) {
				set.add(vertex);
			}

			Collection<Integer> adjacentVertices = graph.getAdjacentVertices(vertex);
			for (Integer adjacentVertex : adjacentVertices) {
				if (level.get(adjacentVertex) == null) {

					List<Integer> list = children.get(vertex);
					if (list == null) {
						list = new ArrayList<Integer>();
						children.put(vertex, list);
					}

					list.add(adjacentVertex);
					level.put(adjacentVertex, Integer.valueOf(vertexLevel + 1));
					parent.put(adjacentVertex, vertex);

					queue.addLast(adjacentVertex);
					vertices.add(adjacentVertex);
					verticesCount++;
				}
			}
		}

		Map<Integer, Double> descendantCosts = new HashMap<Integer, Double>();
		for (int i = radius; i >= 0; i--) {
			List<Integer> verticesOnLevel = levels.get(Integer.valueOf(i));
			for (Integer vertex : verticesOnLevel) {
				{
					increaseMapValue(descendantCosts, vertex, 1);
				}
				
				Integer p = parent.get(vertex);
				if (p != null) {
					increaseMapValue(descendantCosts, p, descendantCosts.get(vertex).doubleValue());
				}
			}
		}

		SpanningTreeImpl tree = new SpanningTreeImpl(verticesCount, parent, level,
				children, root, radius, levels, descendantCosts, vertices);
		return tree;
	}
	
	public static void increaseMapValue(Map<Integer, Double> map, Integer key, double value) {
		Double costs = map.get(key);
		map.put(key, Double.valueOf(costs == null ? value : costs.doubleValue() + value));
	}

	public void outputLevels() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Levels");

			for (Entry<Integer, List<Integer>> entry : levels.entrySet()) {
				StringBuilder builder = new StringBuilder();
				builder.append("Level ").append(entry.getKey()).append(": ");

				List<Integer> list = entry.getValue();
				for (Integer vertex : list) {
					builder.append(vertex).append(" ");
				}

				LOG.debug(builder.toString());
			}
		}
	}

	public void outputAssociatedCosts() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Associated costs");
			for (int i = 0; i < verticesCount; i++) {
				LOG.debug("Cost " + i + ": " + descendantCosts.get(Integer.valueOf(i)));
			}
		}
	}

	public void outputChildren() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Children");
			for (int i = 0; i < verticesCount; i++) {
				StringBuilder builder = new StringBuilder();
				builder.append(i).append(": ");
				List<Integer> vertices = children.get(Integer.valueOf(i));
				if (vertices != null) {
					for (Integer vertex : vertices) {
						builder.append(vertex).append(" ");
					}
				}
				LOG.debug(builder.toString());
			}
		}
	}

	@Override
	public Collection<Integer> getVertices() {
		return vertices;
	}


}
