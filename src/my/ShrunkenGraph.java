package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

public class ShrunkenGraph extends AbstractPlanarGraph{
	
	private final static Logger LOG = Logger.getLogger(ShrunkenGraph.class);
	
	private final PlanarGraph<Integer> sourceGraph;
	private final SpanningTreeImpl sourceTree;
	
	private final Set<Integer> vertices;
	
	private final ShrunkenTree tree;
	private Set<Triangle> triangles = null;
	private final Map<Integer, List<Triangle>> vertexToTriangles;
	private final Map<Integer, Double> descendantCosts;
	
	private final int l0;
	private final int l2;
	
	private final Integer root;
	private final int verticesCount;

	private Integer cutVertex;
	private Set<Integer>[] biconnectedComponents; 
	
	private final Map<Integer, List<Integer>> adjacentList; 
	
	public ShrunkenGraph(PlanarGraph<Integer> sourceGraph, SpanningTreeImpl sourceTree, int l0, int l2) {
		super();
		this.sourceGraph = sourceGraph;
		this.sourceTree = sourceTree;
						
		this.l0 = l0;
		this.l2 = l2 <= sourceTree.getRadius() ? l2 : sourceTree.getRadius() + 1;
		
		this.vertices = new HashSet<Integer>();
		for (int i = l0 + 1; i < l2; i++) {
			vertices.addAll(sourceTree.getVertices(i));
		}

		this.root = Integer.valueOf(-1);
		
		this.adjacentList = new HashMap<Integer, List<Integer>>();
		
		this.tree = new ShrunkenTree();
		this.descendantCosts = new HashMap<Integer, Double>();
		calculateDescedandCosts();
		
		this.verticesCount = calculateVerticesCount();
		this.vertexToTriangles = new HashMap<Integer, List<Triangle>>(); 
		
		try {
			triangulateExternalFace();
		} catch (BorderCrossesItselfException e) {
			cutVertex = e.getVertex();
			calculateBiconnectedComponent();
		}
		
		makePlanarRepresentation();
	}
	
	private void calculateBiconnectedComponent() {
		
		List<Integer> children = tree.getChildren(cutVertex);
		if (children.isEmpty()) {
			throw new IllegalStateException();
		}
		Integer startVertex = children.get(0);

		Set<Integer> biconnectedComponent = new HashSet<Integer>();
		
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(startVertex);
		biconnectedComponent.add(startVertex);
		
		while (!queue.isEmpty()) {
			Integer vertex = queue.removeFirst();
			
			Collection<Integer> adjacentVertices = getAdjacentVertices(vertex);
			for (Integer adjacentVertex : adjacentVertices) {
				if ((!adjacentVertex.equals(cutVertex)) && (!biconnectedComponent.contains(adjacentVertex))) {
					queue.addLast(adjacentVertex);
					biconnectedComponent.add(adjacentVertex);
				}
			}
		}
		
		biconnectedComponents = new Set[2];
		biconnectedComponents[0] = biconnectedComponent;
		
		biconnectedComponents[1] = new HashSet<Integer>(vertices);
		biconnectedComponents[1].remove(cutVertex);
		biconnectedComponents[1].removeAll(biconnectedComponent);
		
		/*
		if (!cutVertex.equals(root)) {
			throw new IllegalStateException("This algorithm is not yet implemented");
		}*/
	}
	
	private void calculateDescedandCosts() {
		
		//code duplicate
		
		//Map<Integer, Integer> descendantCosts = new HashMap<Integer, Integer>();
		for (int i = tree.getRadius(); i >= 0; i--) {
			List<Integer> vertices = tree.getVertices(i);
			for (Integer vertex : vertices) {
				{
					if (!vertex.equals(root)) {
						SpanningTreeImpl.increaseMapValue(descendantCosts, vertex, getWeight(vertex));
					}
				}
				
				Integer p = tree.getParent(vertex);
				if (p != null) {
					SpanningTreeImpl.increaseMapValue(descendantCosts, p, descendantCosts.get(vertex).intValue());
				}
			}
		}
		
	}
	
	private void triangulateExternalFace() throws BorderCrossesItselfException {
		
		calculateTriangles();
		
		//calculate border
		//List<Edge> border = new ArrayList<Edge>();
		Map<Integer, Integer> border = calculateBorder();
		
		if (border.keySet().size() < 3) {
			throw new IllegalStateException("Border contains no more than 3 edges");
		}
		
		Iterator<Integer> iterator = border.keySet().iterator();
		
		Integer startVertex;
		int sum;
		do{
		   startVertex = iterator.next();
		   
		   List<Integer> list = adjacentList.get(startVertex);
		   sum = 0;
		   for (Integer vertex : list) {
			   if (border.containsKey(vertex)) {
				   sum++;
			   }
		   }
			
		}
		   
		while(sum > 2);

		Integer current = border.get(startVertex);
		Integer next = border.get(current);
		
		if ((current == null) || (next == null)) {
			throw new IllegalStateException("Border is inconsistent");
		}
		
		while (!next.equals(startVertex)) {
			addTriangle(new Triangle(startVertex.intValue(), next.intValue(), current.intValue()));
			
			current = next;
			next = border.get(next);
		}
		
		
	}

	private Map<Integer, Integer> calculateBorder() throws BorderCrossesItselfException {
		
		List<Edge> edges = new ArrayList<Edge>();
		
		for (Triangle triangle : triangles) {
			
			int vertex1 = triangle.vertex1;
			int vertex2 = triangle.vertex2;
			int vertex3 = triangle.vertex3;
			
			registerEdge(edges, vertex1, vertex2);
			registerEdge(edges, vertex2, vertex3);
			registerEdge(edges, vertex3, vertex1);
			
		}
		
		Map<Integer, Integer> border = new HashMap<Integer, Integer>();
		
		for (Edge edge : edges) {
			Integer p1 = Integer.valueOf(edge.getP1());
			if (border.get(p1) != null) {
				throw new BorderCrossesItselfException(p1);
			}
			border.put(p1, Integer.valueOf(edge.getP2()));
		}

		return border;
	}

	private static void registerEdge(List<Edge> edges, int vertex1, int vertex2) {
		Edge invertedEdge = new Edge(vertex2, vertex1);
		
		if (edges.contains(invertedEdge)) {
			edges.remove(invertedEdge);
		}else{
			edges.add(invertedEdge.invert());
		}
	}

	@Override
	public Set<Triangle> getTriangles() {
		if (triangles == null) {
			calculateTriangles();
		}
		return triangles;
	}

	private void calculateTriangles() {
		triangles = new HashSet<Triangle>();
		
		for (Triangle triangle : sourceGraph.getTriangles()) {
			int vertex1 = triangle.vertex1;
			int vertex2 = triangle.vertex2;
			int vertex3 = triangle.vertex3;
			
			int level1 = sourceTree.getLevel(Integer.valueOf(vertex1));
			int level2 = sourceTree.getLevel(Integer.valueOf(vertex2));
			int level3 = sourceTree.getLevel(Integer.valueOf(vertex3));
			
			if ((level1 >= l2) || (level2 >= l2) || (level3 >= l2)) {
				continue;
			}
			
			int sum = 0;
			if (level1 <= l0) {
				vertex1 = root.intValue();
				sum++;
			}
			
			if (level2 <= l0) {
				vertex2 = root.intValue();
				sum++;
			}

			if (level3 <= l0) {
				vertex3 = root.intValue();
				sum++;
			}
			
			if (sum == 0) {
				addTriangle(triangle);
			}else if (sum == 1) {
				addTriangle(new Triangle(vertex1, vertex2, vertex3));
			}
			
		}
		
		
	}
	
	private int calculateVerticesCount() {
		
		int sum = 1; //root
		for (int i = l0 + 1; i < l2; i++) {
			sum += sourceTree.getNumberOfVertices(i);
		}
		return sum;
	}

	@Override
	public int getVerticesCount() {
        return verticesCount;
	}
	
	@Override
	public List<Integer> getAdjacentVertices(Integer vertex) {
		
		if (!exist(vertex)) {
			throw new IllegalArgumentException("Vertex " + vertex + " doesn't exist in this graph.");
		}
		
		List<Integer> list = adjacentList.get(vertex);
		
		if (list == null) {
			throw new IllegalStateException("List for vertex " + vertex + " is not calculated");
		}

		return list;
	}
	
	public Separation<Integer> separateByCutVertex() {
		
		int maxSize = 0;
		int maxIndex = -1;
		
		for (int i = 0; i < biconnectedComponents.length; i++) {
			int size = biconnectedComponents[i].size();
			if (size > maxSize) {
				maxSize = size;
				maxIndex = i;
			}
		}
		
		Set<Integer> componentA = new HashSet<Integer>();
		Set<Integer> componentB = new HashSet<Integer>();
		
		for (int i = 0; i < biconnectedComponents.length; i++) {
			if (i == maxIndex) {
				componentA.addAll(biconnectedComponents[i]);
			}else{
				componentB.addAll(biconnectedComponents[i]);
			}
		}
		
		Set<Integer> separator = new HashSet<Integer>();
		separator.add(cutVertex);

		Separation<Integer> separation = new Separation<Integer>(componentA, componentB, separator);
		return separation;
		
	}
	
	public Separation<Integer> separateByCycle(LinkedList<Integer> cycle) {

		Set<Integer> componentA = new HashSet<Integer>();

		Iterator<Integer> iterator = cycle.iterator();
		Integer first = iterator.next();
		
		Integer prev = cycle.getLast();
		Integer current = first;
		Integer next = iterator.next();
		
		//CODE Duplicate
		do {
			if (next.equals(prev)) {
				throw new IllegalStateException("next == prev");
			}
			
			List<Integer> adjacentVertices = getAdjacentVertices(current);
			int size = adjacentVertices.size();
			
			int i = (adjacentVertices.indexOf(next) + 1) % size;
			
			Integer adjacentVertex = adjacentVertices.get(i);
			
			//InOutside inOutside = InOutside.IN;
			while (!adjacentVertex.equals(prev)) {
				
				if (current.equals(tree.getParent(adjacentVertex))) {
					addSubTree(componentA, adjacentVertex);
				}				

				i = (i + 1) % size;
				adjacentVertex = adjacentVertices.get(i);
			}

			prev = current;
			current = next;
			next = iterator.hasNext() ? iterator.next() : first;
		} while (!current.equals(first));
		
		
		Set<Integer> componentB = new HashSet<Integer>(vertices);
		componentB.removeAll(componentA);
		componentB.removeAll(cycle);
		
		Separation<Integer> separation = new Separation<Integer>(componentA, componentB, cycle);
		return separation;
	}
	
	public void addSubTree(Set<Integer> set, Integer vertex) {
		set.add(vertex);
		
		List<Integer> children = tree.getChildren(vertex);
		if (children != null) {
			for (Integer child : children) {
				addSubTree(set, child);
			}
		}
	}
	
	@Override
	public boolean exist(Integer vertex) {
		
		if (root.equals(vertex)) {
			return true;
		}

		int level = sourceTree.getLevel(vertex);
		return (level > l0) && (level < l2);
	}
	
	public ShrunkenTree getTree() {
		return tree;
	}
	
	public Edge getArbitraryNonTreeEdge() {
		//by level 0 is only root, which is not connected with any non-tree edge 
		int level = 1;
		do {
			List<Integer> vertices = tree.getVertices(level);
			for (Integer vertex : vertices) {
				for (Integer adjacentVertex : getAdjacentVertices(vertex)) {
					int adjacentVertexLevel = tree.getLevel(adjacentVertex);
					if (((adjacentVertexLevel == level + 1) || (adjacentVertexLevel == level)) 
							&& (!tree.getChildren(vertex).contains(adjacentVertex))) {
						return new Edge(vertex.intValue(), adjacentVertex.intValue());
					}
				}
			}
			
			level++;
			
		}while (level <= tree.getRadius());
		
		throw new IllegalStateException("No non-tree Edge found");
	}
	
	public class ShrunkenTree implements SpanningTree<Integer>{

		@Override
		public int getVerticesCount() {
			throw new UnsupportedOperationException("Method is not yet implemented");
		}

		@Override
		public int getRadius() {
			return l2 - l0 - 1;
		}

		@Override
		public int getLevel(Integer vertex) {
			if (root.equals(vertex)) {
				return 0;
			}
			
			return sourceTree.getLevel(vertex) - l0;
		}

		@Override
		public List<Integer> getVertices(int level) {
			if (level == 0) {
				return Collections.<Integer>singletonList(root);
			}
			
			if (level > getRadius()) {
				return Collections.<Integer>emptyList();
			}
			
			return sourceTree.getVertices(l0 + level);
		}

		@Override
		public int getNumberOfVertices(int level) {
			return getVertices(level).size();
		}

		@Override
		public Integer getParent(Integer vertex) {
			if (root.equals(vertex)) {
				return null;
			}
			
			if (getLevel(vertex) == 1) {
				return root;
			}
			
			return sourceTree.getParent(vertex);
		}

		@Override
		public List<Integer> getChildren(Integer vertex) {
			
			if (root.equals(vertex)) {
				return getVertices(1);
			}
			
			if (sourceTree.getLevel(vertex) == l2 - 1) {
				return Collections.<Integer>emptyList();
			}
			
			return sourceTree.getChildren(vertex);
		}

		@Override
		public Integer getRoot() {
			return root;
		}

		@Override
		public double getDescendantCosts(Integer vertex) {
			return descendantCosts.get(vertex).doubleValue();
		}

		@Override
		public Collection<Integer> getVertices() {
			throw new IllegalStateException("This method is not yet implemented");
		}
		
	}

	@Override
	public Integer getStartVertex() {
		return root;
	}

	@Override
	public List<Triangle> getTriangles(Integer vertex) {
		return vertexToTriangles.get(vertex);
	}
	
	public Integer getCutVertex() {
		return cutVertex;
	}
	
	/**
	 * 
	 * @param edge  non tree edge
	 * @return
	 */
	public LinkedList<Integer> findCycle(Edge edge) {
		LinkedList<Integer> cycle = new LinkedList<Integer>();
		
		Integer p1 = Integer.valueOf(edge.getP1());
		Integer p2 = Integer.valueOf(edge.getP2());
		
		int level1 = tree.getLevel(p1);
		int level2 = tree.getLevel(p2);
		
		if ((level1 - level2 > 1) || (level1 - level2 < -1)) {
			throw new IllegalArgumentException("Vertices should be of the same level or of adjacent levels, level1 = " + level1 + "; level2 = " + level2);
		}
		
		cycle.addFirst(p1);
		cycle.addLast(p2);
		
		//int level;
		if (level1 > level2) {
			p1 = tree.getParent(p1);
			cycle.addFirst(p1);
			//level = level2;
			
		}else if (level1 < level2){
			p2 = tree.getParent(p2);
			cycle.addLast(p2);
			//level = level1;
			
		}
		
		while (!p1.equals(p2)) {
			p1 = tree.getParent(p1);
			p2 = tree.getParent(p2);
			
			if ((p1 == null) || (p2 == null)) {
				throw new IllegalStateException("Root reached but no cycle found");
			}
			
			cycle.addFirst(p1);
			if (!p1.equals(p2)) {
				cycle.addLast(p2);
			}
			
		}
		
		return cycle;
	}

	@Override
	protected void setAdjacencyList(Integer vertex, List<Integer> list) {
		adjacentList.put(vertex, list);
	}

	@Override
	public void outputAdjacencyLists() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("AdjacencyLists");
			for (int i = -1; i < sourceGraph.getVerticesCount(); i++) {

				if (exist(Integer.valueOf(i))) {

					StringBuilder builder = new StringBuilder();
					builder.append(i).append(": ");

					List<Integer> list = getAdjacentVertices(Integer.valueOf(i));
					for (Integer vertex : list) {
						builder.append(vertex).append(" ");
					}

					LOG.debug(builder.toString());
				}
			}
		}
	}	
	

	@Override
	public void addTriangle(Triangle triangle) {
		super.addTriangle(triangle);
		
		triangles.add(triangle);
		
		registerTriangle(triangle, Integer.valueOf(triangle.vertex1));
		registerTriangle(triangle, Integer.valueOf(triangle.vertex2));
		registerTriangle(triangle, Integer.valueOf(triangle.vertex3));
	}

	private void registerTriangle(Triangle triangle, Integer vertex) {

		List<Triangle> list = vertexToTriangles.get(vertex);
		if (list == null) {
			list = new ArrayList<Triangle>();
			vertexToTriangles.put(vertex, list);
		}
		list.add(triangle);
	}

	@Override
	protected void registerEdge(Integer vertex1, Integer vertex2) {
				
		List<Integer> list = adjacentList.get(vertex1);
		if (list == null) {
			list = new ArrayList<Integer>();
			adjacentList.put(vertex1, list);
		}
		
		if (!list.contains(vertex2)) {
			list.add(vertex2);
		}
	}

	@Override
	public double getWeight(Integer vertex) {
		if (root.equals(vertex)) {
			return 0;
		}
		
		return sourceGraph.getWeight(vertex);
	}

	@Override
	public Collection<Integer> getVertices() {
		throw new IllegalStateException("This moethod is not yet implemented");
	}
}
