package my;

import java.util.Collection;

public interface SeparationIfc<V> {

	public Collection<V> getSeparator();

}