package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class AbstractPlanarGraph implements PlanarGraph<Integer> {
	
	protected void makePlanarRepresentation() {
		
		Collection<Triangle> triangles = getTriangles();
		
		if (triangles.size() == 0) {
			return;
		}
		
		Set<Triangle> trianglesProcessed = new HashSet<Triangle>();
		Set<Integer> verticesProcessed = new HashSet<Integer>();
		
		LinkedList<Integer> queue = new LinkedList<Integer>(); 
		
		Triangle startTriangle = triangles.iterator().next();
		
		trianglesProcessed.add(startTriangle);
		
		queue.add(Integer.valueOf(startTriangle.vertex1));
		queue.add(Integer.valueOf(startTriangle.vertex2));
		queue.add(Integer.valueOf(startTriangle.vertex3));
		
		while (!queue.isEmpty()) {
			Integer vertexInt = queue.removeFirst();
			if (verticesProcessed.contains(vertexInt)) {
				continue;
			}
			
			int vertex = vertexInt.intValue();
			
			List<Triangle> list = getTriangles(vertexInt);
			
			Triangle processed = null;
			for (Triangle triangle : list) {
				if (trianglesProcessed.contains(triangle)) {
					processed = triangle;
				}
			}
			
			if (processed == null) {
				throw new IllegalStateException("No triangle found for vertex " + vertexInt);
			}
			
		    LinkedList<Integer> arrangedList = new LinkedList<Integer>();
		    int mostCCW1 = processed.getCounterClockwiseVertex(vertex);
		    int mostCW1 = processed.getClockwiseVertex(vertex);
		    
		    int mostCCW2 = mostCW1;
		    int mostCW2 = mostCCW1;
		    
			{
				Integer mostCCW1Int = Integer.valueOf(mostCCW1);
				Integer mostCW1Int = Integer.valueOf(mostCW1);

				arrangedList.addFirst(mostCCW1Int);
				arrangedList.addLast(mostCW1Int);

				if (!verticesProcessed.contains(mostCCW1Int)) {
					queue.addLast(mostCCW1Int);
				}

				if (!verticesProcessed.contains(mostCW1Int)) {
					queue.addLast(mostCW1Int);
				}
			}
			
			boolean flag = true;
			while(flag) {

				//clockwise transfer
				boolean cwFound = false; 
				for (Iterator<Triangle> iterator = list.iterator(); iterator.hasNext() && (!cwFound);) {
					Triangle triangle = iterator.next();
					if (triangle.contains(mostCW1)
							&& (!triangle.contains(mostCW2))) {
						cwFound = true;
						int third = triangle.third(vertex, mostCW1);
						mostCW2 = mostCW1;
						mostCW1 = third;
						
						if (mostCW1 == mostCCW1) {
							flag = false;
						}
						
						if (flag) {
							Integer mostCW1Int = Integer.valueOf(mostCW1);
							if (!verticesProcessed.contains(mostCW1Int)) {
								queue.addLast(mostCW1Int);
							}
							arrangedList.addLast(mostCW1Int);
							if (!trianglesProcessed.contains(triangle)) {
								triangle.rearrange(vertex, mostCW2, mostCW1);
								trianglesProcessed.add(triangle);
							}
						}
					}
				}
				
				
				boolean ccwFound = false; 
				if (flag) {
				//counterclockwise transfer
				for (Iterator<Triangle> iterator = list.iterator(); iterator.hasNext() && (!ccwFound);) {
					 Triangle triangle = iterator.next();
					 if (triangle.contains(mostCCW1) && (!triangle.contains(mostCCW2))) {
							ccwFound = true;
							int third = triangle.third(vertex, mostCCW1);
							mostCCW2 = mostCCW1;
							mostCCW1 = third;

							if (mostCW1 == mostCCW1) {
								flag = false;
							}
							
							if (flag) {
								Integer mostCCW1Int = Integer.valueOf(mostCCW1);
								if (!verticesProcessed.contains(mostCCW1Int)) {
									queue.addLast(mostCCW1Int);
								}
								arrangedList.addFirst(mostCCW1Int);
								if (!trianglesProcessed.contains(triangle)) {
									triangle.rearrange(vertex, mostCCW1, mostCCW2);
									trianglesProcessed.add(triangle);
								}
							}
					}
				}
				}
				
				if (((!cwFound) && (!ccwFound)) || (mostCW1 == mostCCW1)) {
					flag = false;
				}
			}
			
			setAdjacencyList(Integer.valueOf(vertex), new ArrayList<Integer>(arrangedList));
			verticesProcessed.add(vertexInt);
		}
	}
	
	public void addTriangle(Triangle triangle) {

		Integer n2 = Integer.valueOf(triangle.vertex2);
		Integer n3 = Integer.valueOf(triangle.vertex3);
		Integer n1 = Integer.valueOf(triangle.vertex1);

		registerEdge(n1, n2);
		registerEdge(n1, n3);

		registerEdge(n2, n1);
		registerEdge(n2, n3);

		registerEdge(n3, n1);
		registerEdge(n3, n2);
		
	}
	
	protected abstract void setAdjacencyList(Integer vertex, List<Integer> list);

	protected abstract void registerEdge(Integer vertex1, Integer vertex2);

}
