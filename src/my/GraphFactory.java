package my;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GraphFactory {

	/*
	public static Coordinates createRectangle(int n) {
		return createRectangle(n, n);
	}*/
	public static PlanarGraphImpl createRectangle(int n, int m) {

		List<Triangle> triangles = new ArrayList<Triangle>();
    	
    	for (int i = 0; i < m - 1; i++) {
    		for (int j = 0; j < n - 1; j++) {
    			int vertex1 = i * n + j;
    			int vertex2 = i * n + j + 1;
    			int vertex3 = (i + 1) * n + j;
    			int vertex4 = (i + 1) * n + j + 1;
    			triangles.add(new Triangle(vertex1, vertex2, vertex3));
    			triangles.add(new Triangle(vertex2, vertex3, vertex4));
    		}
    	}
    	
    	PlanarGraphImpl graph = new PlanarGraphImpl(n * m, triangles);
		return graph;
	};
	
	public static Coordinates createCoordinates(int n, int m) {
		Coordinates coordinates = new Coordinates(n * m);

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				// coordinates.setXY(i * n + j, i, j);
				coordinates.setXY(i * n + j, j, i);
			}
		}

		coordinates.setMaxX(n);
		coordinates.setMaxY(m);

		return coordinates;
	}
	
	public static PlanarGraphImpl createSample1() { 

		List<Triangle> triangles = new ArrayList<Triangle>();
		
		triangles.add(new Triangle(0, 1, 2));
		triangles.add(new Triangle(0, 2, 3));
		triangles.add(new Triangle(0, 3, 4));
		triangles.add(new Triangle(1, 2, 3));
		triangles.add(new Triangle(1, 3, 4));
		triangles.add(new Triangle(1, 4, 5));
		triangles.add(new Triangle(5, 4, 6));
		triangles.add(new Triangle(1, 5, 7));
		triangles.add(new Triangle(5, 6, 7));
		triangles.add(new Triangle(4, 6, 7));
		
		PlanarGraphImpl graph = new PlanarGraphImpl(8, triangles);
    	return graph;
	}
	
	public static PlanarGraph<Integer> createSample2() {
		Set<Integer> vertices = new HashSet<Integer>();
		// 1221, 1223, 1222, 1371, 1124, 1122, 1123, 1224, 1169, 1271, 1321,
		// 1322, 1170, 1171, 1172, 1173, 1174, 1074, 1073, 1272, 1273

		
		vertices.add(Integer.valueOf(1221));
		vertices.add(Integer.valueOf(1223));
		vertices.add(Integer.valueOf(1222));
		vertices.add(Integer.valueOf(1371));
		vertices.add(Integer.valueOf(1124));
		vertices.add(Integer.valueOf(1122));
		vertices.add(Integer.valueOf(1123));
		vertices.add(Integer.valueOf(1224));
		vertices.add(Integer.valueOf(1169));
		vertices.add(Integer.valueOf(1271));
		vertices.add(Integer.valueOf(1321));
		
		vertices.add(Integer.valueOf(1322));
		vertices.add(Integer.valueOf(1170));
		vertices.add(Integer.valueOf(1171));
		vertices.add(Integer.valueOf(1172));
		vertices.add(Integer.valueOf(1173));
		vertices.add(Integer.valueOf(1174));

		vertices.add(Integer.valueOf(1074));
		vertices.add(Integer.valueOf(1073));
		vertices.add(Integer.valueOf(1272));
		vertices.add(Integer.valueOf(1273));

		SubGraph graph = new SubGraph(GraphFactory.createRectangle(50,
				50), vertices);
		
		return graph;
		
	}
	
	public static PlanarGraph<Integer> createSample3() {
		Set<Integer> vertices = new HashSet<Integer>();

		//1916, 2205, 2206, 2207, 1909, 1908, 1911, 1910, 1116, 1611, 1612, 1613, 1614, 1615, 2708, 2709, 2710, 2711, 2705, 2706, 2707, 2216, 2211, 1616, 2210, 2209, 2208, 2215, 2214, 2213, 2212, 2511, 2510, 2509, 2508, 2507, 2506, 2505, 2512, 2513, 1816, 1812, 1811, 1810, 1809, 1314, 1315, 1316, 2306, 2307, 2612, 2305, 2310, 2611, 2311, 2610, 1216, 2609, 2308, 2309, 2608, 2314, 2315, 2312, 2313, 2016, 2606, 2607, 2605, 2007, 2107, 2010, 2106, 2011, 2008, 2009, 2111, 2014, 2015, 2110, 2109, 2108, 1516, 1513, 1512, 1515, 1514, 1711, 1710, 2116, 1716, 1714, 2112, 1415, 1715, 2113, 1414, 2114, 1712, 1413, 2115, 1713, 1416, 2405, 2406, 2407, 2412, 2413, 2414, 2408, 2409, 2410, 2411, 1215
		vertices.add(Integer.valueOf(1916));
		vertices.add(Integer.valueOf(2205));
		vertices.add(Integer.valueOf(2206));
		vertices.add(Integer.valueOf(2207));
		vertices.add(Integer.valueOf(1909));
		vertices.add(Integer.valueOf(1908));
		vertices.add(Integer.valueOf(1911));
		vertices.add(Integer.valueOf(1910));
		vertices.add(Integer.valueOf(1116));
		vertices.add(Integer.valueOf(1611));
		vertices.add(Integer.valueOf(1612));
		vertices.add(Integer.valueOf(1613));
		vertices.add(Integer.valueOf(1614));
		vertices.add(Integer.valueOf(1615));
		vertices.add(Integer.valueOf(2708));
		vertices.add(Integer.valueOf(2709));
		vertices.add(Integer.valueOf(2710));
		vertices.add(Integer.valueOf(2711));
		vertices.add(Integer.valueOf(2705));
		vertices.add(Integer.valueOf(2706));
		vertices.add(Integer.valueOf(2707));
		vertices.add(Integer.valueOf(2216));
		vertices.add(Integer.valueOf(2211));
		vertices.add(Integer.valueOf(1616));
		vertices.add(Integer.valueOf(2210));
		vertices.add(Integer.valueOf(2209));
		vertices.add(Integer.valueOf(2208));
		vertices.add(Integer.valueOf(2215));
		vertices.add(Integer.valueOf(2214));
		vertices.add(Integer.valueOf(2213));
		vertices.add(Integer.valueOf(2212));
		vertices.add(Integer.valueOf(2511));
		vertices.add(Integer.valueOf(2510));
		vertices.add(Integer.valueOf(2509));
		vertices.add(Integer.valueOf(2508));
		vertices.add(Integer.valueOf(2507));
		vertices.add(Integer.valueOf(2506));
		vertices.add(Integer.valueOf(2505));
		vertices.add(Integer.valueOf(2512));
		vertices.add(Integer.valueOf(2513));
		vertices.add(Integer.valueOf(1816));
		vertices.add(Integer.valueOf(1812));
		vertices.add(Integer.valueOf(1811));
		vertices.add(Integer.valueOf(1810));
		vertices.add(Integer.valueOf(1809));
		vertices.add(Integer.valueOf(1314));
		vertices.add(Integer.valueOf(1315));
		vertices.add(Integer.valueOf(1316));
		vertices.add(Integer.valueOf(2306));
		vertices.add(Integer.valueOf(2307));
		vertices.add(Integer.valueOf(2612));
		vertices.add(Integer.valueOf(2305));
		vertices.add(Integer.valueOf(2310));
		vertices.add(Integer.valueOf(2611));
		vertices.add(Integer.valueOf(2311));
		vertices.add(Integer.valueOf(2610));
		vertices.add(Integer.valueOf(1216));
		vertices.add(Integer.valueOf(2609));
		vertices.add(Integer.valueOf(2308));
		vertices.add(Integer.valueOf(2309));
		vertices.add(Integer.valueOf(2608));
		vertices.add(Integer.valueOf(2314));
		vertices.add(Integer.valueOf(2315));
		vertices.add(Integer.valueOf(2312));
		vertices.add(Integer.valueOf(2313));
		vertices.add(Integer.valueOf(2016));
		vertices.add(Integer.valueOf(2606));
		vertices.add(Integer.valueOf(2607));
		vertices.add(Integer.valueOf(2605));
		vertices.add(Integer.valueOf(2007));
		vertices.add(Integer.valueOf(2107));
		vertices.add(Integer.valueOf(2010));
		vertices.add(Integer.valueOf(2106));
		vertices.add(Integer.valueOf(2011));
		vertices.add(Integer.valueOf(2008));
		vertices.add(Integer.valueOf(2009));
		vertices.add(Integer.valueOf(2111));
		vertices.add(Integer.valueOf(2014));
		vertices.add(Integer.valueOf(2015));
		vertices.add(Integer.valueOf(2110));
		vertices.add(Integer.valueOf(2109));
		vertices.add(Integer.valueOf(2108));
		vertices.add(Integer.valueOf(1516));
		vertices.add(Integer.valueOf(1513));
		vertices.add(Integer.valueOf(1512));
		vertices.add(Integer.valueOf(1515));
		vertices.add(Integer.valueOf(1514));
		vertices.add(Integer.valueOf(1711));
		vertices.add(Integer.valueOf(1710));
		vertices.add(Integer.valueOf(2116));
		vertices.add(Integer.valueOf(1716));
		vertices.add(Integer.valueOf(1714));
		vertices.add(Integer.valueOf(2112));
		vertices.add(Integer.valueOf(1415));
		vertices.add(Integer.valueOf(1715));
		vertices.add(Integer.valueOf(2113));
		vertices.add(Integer.valueOf(1414));
		vertices.add(Integer.valueOf(2114));
		vertices.add(Integer.valueOf(1712));
		vertices.add(Integer.valueOf(1413));
		vertices.add(Integer.valueOf(2115));
		vertices.add(Integer.valueOf(1713));
		vertices.add(Integer.valueOf(1416));
		vertices.add(Integer.valueOf(2405));
		vertices.add(Integer.valueOf(2406));
		vertices.add(Integer.valueOf(2407));
		vertices.add(Integer.valueOf(2412));
		vertices.add(Integer.valueOf(2413));
		vertices.add(Integer.valueOf(2414));
		vertices.add(Integer.valueOf(2408));
		vertices.add(Integer.valueOf(2409));
		vertices.add(Integer.valueOf(2410));
		vertices.add(Integer.valueOf(2411));
		vertices.add(Integer.valueOf(1215));

		SubGraph graph = new SubGraph(GraphFactory.createRectangle(100,
				100), vertices);
		
		graph.setStartVertex(Integer.valueOf(1916));
		
		return graph;
		
	}
	
}
