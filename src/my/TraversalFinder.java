package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class TraversalFinder {

	private final static Logger LOG = Logger.getLogger(TraversalFinder.class);

	private final SeparatorFinderIfc finder;
	private final RenderingTreeAdjuster adjuster;

	public TraversalFinder(SeparatorFinderIfc finder,
			RenderingTreeAdjuster adjuster) {
		super();
		this.finder = finder;
		this.adjuster = adjuster;
	}

	public static int evaluatePathCosts(List<Triangle> path, int cashSize) {

		Cash cash = new Cash(cashSize);

		for (Triangle triangle : path) {
			// Triangle triangle = graph.getTriangles().get(i.intValue());

			if (LOG.isDebugEnabled()) {
				LOG.debug(triangle);
			}

			cash.put(Integer.valueOf(triangle.vertex1),
					Integer.valueOf(triangle.vertex2),
					Integer.valueOf(triangle.vertex3));
		}

		return cash.getEvaluations();
	}

	private static int borderWidth(int cashSize) {
		return (cashSize + 1) / 2;
	}

	private static void processTriangle(PlanarGraph graph, List<Triangle> path,
			Cash cash, Triangle triangle) {

		if (LOG.isDebugEnabled()) {
			LOG.debug(triangle);
		}

		// graph.removeTriangle(triangle);
		path.add(triangle);

		cash.put(Integer.valueOf(triangle.vertex1), Integer.valueOf(triangle.vertex2), Integer.valueOf(triangle.vertex3));

	};

	public static class Cash {
		private final List<Integer> cash;
		private final int size;

		private int evaluations = 0;

		public Cash(int size) {
			super();
			this.size = size;
			cash = new ArrayList<Integer>();
		}

		public void put(Integer value1, Integer value2, Integer value3) {

			add(value1);
			add(value2);
			add(value3);

			adjustSize();
		}

		public void put(Integer value) {
			add(value);
			adjustSize();
		}

		private void adjustSize() {
			while (cash.size() > size) {
				cash.remove(cash.size() - 1);
			}
		}

		private void add(Integer value) {
			if (cash.contains(value)) {
				cash.remove(value);
			} else {
				evaluations++;
			}
			cash.add(0, value);
		}

		public int getEvaluations() {
			return evaluations;
		}

	}

	public RenderingTree generateRenderingTree(PlanarGraph<Integer> graph) {

		RenderingTree tree = new RenderingTree(graph.getVertices());

		if (graph.getVerticesCount() == 0) {
			return tree;
		}

		SeparationIfc<Integer> separation = finder.findSeparator(graph);

		RenderingTree child1 = generateRenderingTree(finder.generateSubGraphA(
				graph, separation));
		RenderingTree child2 = generateRenderingTree(finder.generateSubGraphB(
				graph, separation));

		tree.setSeparator(separation.getSeparator());
		tree.setChild1(child1);
		tree.setChild2(child2);

		return tree;
	}

	public List<Triangle> generateMinimumStackRendering(
			PlanarGraph<Integer> graph) {

		LinkedList<Integer> stack = new LinkedList<Integer>();
		List<Triangle> rendering = new LinkedList<Triangle>();

		RenderingTree tree = generateRenderingTree(graph);

		adjuster.adjustRenderingTree(tree, graph);

		tree.log(Level.DEBUG);

		generateRendering(graph, tree, stack, rendering);

		return rendering;
	}

	static void addSeparatorTriangles(PlanarGraph<Integer> sourceGraph,
			LinkedList<Integer> stack, List<Triangle> rendering,
			Collection<Integer> separator) {
		List<Triangle> toAdd = new ArrayList<Triangle>();
		for (Integer vertex : separator) {

			List<Triangle> triangles = sourceGraph.getTriangles(vertex);
			for (Triangle triangle : triangles) {
				if (!toAdd.contains(triangle)) {
					int second = triangle.second(vertex.intValue());
					int third = triangle.third(vertex.intValue(), second);

					if (stack.contains(Integer.valueOf(second))
							&& stack.contains(Integer.valueOf(third))) {
						toAdd.add(triangle);
						if (LOG.isDebugEnabled()) {
							LOG.debug("Add triangle: " + triangle);
						}
					}
				}
			}
		}

		rendering.addAll(toAdd);
	}

	public static void generateRendering(PlanarGraph<Integer> sourceGraph,
			RenderingTree tree, LinkedList<Integer> stack,
			List<Triangle> rendering) {

		if (LOG.isDebugEnabled()) {
			LOG.debug("Tree component: " + tree.getComponent());
		}

		if (tree.isEmpty()) { 
			 return; 
	    }

		// PUSH
		Collection<Integer> separator = tree.getSeparator();

		if (separator != null) {
			for (Integer vertex : separator) {
				stack.addFirst(vertex);
			}
		}

		if (tree.hasChild1()) {
			generateRendering(sourceGraph, tree.getChild1(), stack, rendering);
		}

		if (separator != null) {
			addSeparatorTriangles(sourceGraph, stack, rendering, separator);
		}

		if (tree.hasChild2()) {
			generateRendering(sourceGraph, tree.getChild2(), stack, rendering);
		}

		// POP
		if (separator != null) {
			while ((!stack.isEmpty()) && (separator.contains(stack.getFirst()))) {
				stack.removeFirst();
			}
		}
	}

}
