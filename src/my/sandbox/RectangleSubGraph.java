package my.sandbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import my.PlanarGraph;
import my.Triangle;

public class RectangleSubGraph implements PlanarGraph<Integer>{

	private final int x1;
	private final int y1;
	private final int x2;
	private final int y2;

	private final int width;
	private final int height;
	
	private final Integer[][] vertices;
	private final List<Triangle> triangles;
	private final List[][] vertexToTriangles;
	
	public RectangleSubGraph(int sourceX, int sourceY, int x1, int y1, int x2, int y2) {
		this.width = sourceX;
		this.height = sourceY;
		
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		
		vertices = new Integer[x2 - x1 + 1][y2 - y1 + 1];
		vertexToTriangles = new List[x2 - x1 + 1][y2 - y1 + 1];
		triangles = new ArrayList<Triangle>();
		
		for (int i = x1; i < x2 + 1; i++) {
			for (int j = y1; j < y2 + 1; j++) {
				vertices[i - x1][j - y1] = Integer.valueOf(i + j * (this.width + 1));
				vertexToTriangles[i - x1][j - y1] = new ArrayList<Triangle>();

				if ((i > x1) && (j > y1)) {
					{
						Triangle triangle = new Triangle(vertices[i - x1][j - y1].intValue(),
								vertices[i - x1][j - y1 - 1].intValue(), vertices[i - x1 - 1][j
										- y1 - 1].intValue());
						triangles.add(triangle);
						vertexToTriangles[i - x1][j - y1].add(triangle);
						vertexToTriangles[i - x1][j - y1 - 1].add(triangle);
						vertexToTriangles[i - x1 - 1][j - y1 - 1].add(triangle);
					}
					
					{
						Triangle triangle = new Triangle(vertices[i - x1][j - y1].intValue(),
								vertices[i - x1 - 1][j - y1].intValue(), vertices[i - x1 - 1][j
										- y1 - 1].intValue());
						triangles.add(triangle);
						vertexToTriangles[i - x1][j - y1].add(triangle);
						vertexToTriangles[i - x1 - 1][j - y1].add(triangle);
						vertexToTriangles[i - x1 - 1][j - y1 - 1].add(triangle);
					}
				}
			}
		}
		
	}

	@Override
	public int getVerticesCount() {
		return (x2 - x1 + 1) * (y2 - y1 + 1);
	}

	@Override
	public Integer getStartVertex() {
		return vertices[0][0];
	}

	@Override
	public Collection<Integer> getVertices() {
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < x2 - x1 + 1; i++) {
			list.addAll(Arrays.asList(vertices[i]));
		}
		return list;
	}

	@Override
	public List<Integer> getAdjacentVertices(Integer vertex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean exist(Integer vertex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Triangle> getTriangles() {
		return triangles;
	}

	@Override
	public List<Triangle> getTriangles(Integer vertex) {

		int x = vertex.intValue() % (width + 1);
		int y = vertex.intValue() / (width + 1);
		
		if (x >= x2 + 1) {
			throw new IllegalStateException();
		}

		if (y >= y2 + 1) {
			throw new IllegalStateException();
		}
		
		return vertexToTriangles[x - x1][y - y1];
	}

	@Override
	public void outputAdjacencyLists() {
		throw new UnsupportedOperationException();
	}

	@Override
	public double getWeight(Integer vertex) {
		return 1.0 / ((double)getVerticesCount());
	}

	public int getX1() {
		return x1;
	}

	public int getY1() {
		return y1;
	}

	public int getX2() {
		return x2;
	}

	public int getY2() {
		return y2;
	}

	public int getSourceX() {
		return width;
	}

	public int getSourceY() {
		return height;
	}
	
	public int getWidth() {
		return x2 - x1;
	}
	
	public int getHeight() {
		return y2 - y1;
	}
	
	public Integer getVertex(int x, int y) {
		return vertices[x - x1][y - y1];
	}

}
