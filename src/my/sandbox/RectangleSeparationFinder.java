package my.sandbox;

import java.util.HashSet;
import java.util.Set;

import my.PlanarGraph;
import my.SeparationIfc;
import my.SeparatorFinderIfc;

public class RectangleSeparationFinder implements SeparatorFinderIfc {

	@Override
	public SeparationIfc<Integer> findSeparator(PlanarGraph<Integer> graph) {
		RectangleSubGraph recGraph = (RectangleSubGraph) graph;
		
		int x = -1;
		int y = -1;
		
		Set<Integer> separator = new HashSet<Integer>();
		
		
		if ((recGraph.getWidth() == 1) && (recGraph.getHeight() == 1)) {
			
			separator.add(recGraph.getVertex(recGraph.getX1(), recGraph.getY1()));
			separator.add(recGraph.getVertex(recGraph.getX2(), recGraph.getY2()));
			
			return new Diagonal2x2Separation(separator);
			
		}
		
		if (recGraph.getWidth() > recGraph.getHeight()) {
			x = (recGraph.getX1() + recGraph.getX2()) / 2;
			
			for (int i = recGraph.getY1(); i < recGraph.getY2() + 1; i++) {
				separator.add(recGraph.getVertex(x, i));
			}
		}else{
			y = (recGraph.getY1() + recGraph.getY2()) / 2;
			
			for (int i = recGraph.getX1(); i < recGraph.getX2() + 1; i++) {
				separator.add(recGraph.getVertex(i, y));
			}
		}
		
		RectangleSeparation separation = new RectangleSeparation(x, y, separator);
		return separation;
	}

	@Override
	public PlanarGraph<Integer> generateSubGraphA(PlanarGraph<Integer> graph,
			SeparationIfc<Integer> separation) {
		
		RectangleSubGraph recGraph = (RectangleSubGraph) graph;
		
		if (separation instanceof Diagonal2x2Separation) {
			return new RectangleSubGraph(recGraph.getSourceX(), recGraph.getSourceY(), 
					recGraph.getX1(), recGraph.getY2(), 
					recGraph.getX1(), recGraph.getY2());
		}
		
		RectangleSeparation recSeparation = (RectangleSeparation) separation;
		
		int x = recSeparation.getX() != -1 ? recSeparation.getX() : recGraph.getX2() + 1; 
		int y = recSeparation.getY() != -1 ? recSeparation.getY() : recGraph.getY2() + 1; 
		
		return new RectangleSubGraph(recGraph.getSourceX(), recGraph.getSourceY(), 
				recGraph.getX1(), recGraph.getY1(), x - 1, y - 1);
	}

	@Override
	public PlanarGraph<Integer> generateSubGraphB(PlanarGraph<Integer> graph,
			SeparationIfc<Integer> separation) {

		RectangleSubGraph recGraph = (RectangleSubGraph) graph;

		if (separation instanceof Diagonal2x2Separation) {
			return new RectangleSubGraph(recGraph.getSourceX(), recGraph.getSourceY(), 
					recGraph.getX2(), recGraph.getY1(), 
					recGraph.getX2(), recGraph.getY1());
		}
		
		RectangleSeparation recSeparation = (RectangleSeparation) separation;
		
		int x = recSeparation.getX() != -1 ? recSeparation.getX() : recGraph.getX1() - 1; 
		int y = recSeparation.getY() != -1 ? recSeparation.getY() : recGraph.getY1() - 1; 
		
		return new RectangleSubGraph(recGraph.getSourceX(), recGraph.getSourceY(), 
				x + 1, y + 1, recGraph.getX2(), recGraph.getY2());
	}

}
