package my.sandbox;

import java.util.Collection;

import my.SeparationIfc;

public class RectangleSeparation implements SeparationIfc<Integer> {

	private final int x;
	private final int y;
	
	private final Collection<Integer> separator;
	
	public RectangleSeparation(int x, int y, Collection<Integer> separator) {
		super();
		this.x = x;
		this.y = y;
		this.separator = separator;
	}

	@Override
	public Collection<Integer> getSeparator() {
		return separator;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
