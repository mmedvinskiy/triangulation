package my.sandbox;

import java.util.Collection;

import my.SeparationIfc;

public class Diagonal2x2Separation implements SeparationIfc<Integer>{

	private final Collection<Integer> separator;
	
	public Diagonal2x2Separation(Collection<Integer> separator) {
		super();
		this.separator = separator;
	}

	@Override
	public Collection<Integer> getSeparator() {
		return separator;
	}

}
