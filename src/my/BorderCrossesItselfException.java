package my;

public class BorderCrossesItselfException extends Exception{
	
	private final Integer vertex;

	public BorderCrossesItselfException(Integer vertex) {
		super();
		this.vertex = vertex;
	}

	public Integer getVertex() {
		return vertex;
	}

}
