package my;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

public class SubGraph implements PlanarGraph<Integer> {

	private final static Logger LOG = Logger.getLogger(SubGraph.class);
	
	protected final PlanarGraph<Integer> sourceGraph;
	protected final Collection<Integer> vertices;

	private Set<Triangle> triangles = null;

	protected final Map<Integer, List<Triangle>> vertexToTriangles;

	protected final double weight;

	protected Integer startVertex = null;
	
	public SubGraph(PlanarGraph<Integer> sourceGraph, Collection<Integer> vertices) {
		super();
		this.sourceGraph = sourceGraph;
		this.vertices = vertices;
		
		vertexToTriangles = new HashMap<Integer, List<Triangle>>();
		
		weight = 1.0 / ((double)vertices.size());
	}

	@Override
	public int getVerticesCount() {
		return vertices.size();
	}

	@Override
	public List<Integer> getAdjacentVertices(Integer vertex) {
		List<Integer> adjacentVertices = sourceGraph.getAdjacentVertices(vertex);
		List<Integer> list = new ArrayList<Integer>();
		for (Integer adjacentIntegerertex : adjacentVertices) {
			if (vertices.contains(adjacentIntegerertex)) {
				list.add(adjacentIntegerertex);
			}
		}
		return list;
	}

	@Override
	public boolean exist(Integer vertex) {
		return vertices.contains(vertex);
	}

	@Override
	public void outputAdjacencyLists() {
		if (LOG.isDebugEnabled()) {
			for (Integer vertex : vertices) {
				StringBuilder builder = new StringBuilder();
				builder.append(vertex).append(": ");

				List<Integer> list = getAdjacentVertices(vertex);
				for (Integer v : list) {
					builder.append(v).append(" ");
				}

				LOG.debug(builder.toString());
			}
		}
	}

	@Override
	public Integer getStartVertex() {
		if (startVertex != null) {
			return startVertex;
		}
		return vertices.iterator().next();
	}

	@Override
	public List<Triangle> getTriangles(Integer vertex) {
		List<Triangle> triangles = vertexToTriangles.get(vertex);
		if (triangles == null) {
			triangles = calculateTriangles(vertex);
			vertexToTriangles.put(vertex, triangles);
		}
		return triangles;
	}

	private List<Triangle> calculateTriangles(Integer vertex) {
		List<Triangle> triangles = sourceGraph.getTriangles(vertex);
		
		List<Triangle> result = new ArrayList<Triangle>();
		
		for (Triangle triangle : triangles) {
			if (exist(Integer.valueOf(triangle.vertex1)) && exist(Integer.valueOf(triangle.vertex2)) && exist(Integer.valueOf(triangle.vertex3)))
			{
				result.add(triangle);
			}
		}
		return result;
	}

	@Override
	public Set<Triangle> getTriangles() {
		if (triangles == null) {
			calculateTriangles();
		}
		return triangles;
	}

	private void calculateTriangles() {
		triangles = new HashSet<Triangle>();
		
		for (Integer vertex : vertices) {
			triangles.addAll(getTriangles(vertex));
		}
		
	}

	public double getWeight(Integer vertex) {
		return weight;
	}

	@Override
	public Collection<Integer> getVertices() {
		return vertices;
	}

	public void setStartVertex(Integer startVertex) {
		this.startVertex = startVertex;
	}

}
